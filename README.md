# php-extended/php-ldap-object
A library that implements the php-ldap-interface package

![coverage](https://gitlab.com/php-extended/php-ldap-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ldap-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ldap-object ^8`


## Basic Usage

This library is based on the ActiveRecord pattern, inspired by the implementation
of the Yii framework for database related record classes, without the validation
part (which should be done with another part of your favorite framework form
component).

For it to work, you need to extend the `PhpExtended\Ldap\LdapRecord` class
with your model classes. The model classes will have a mandatory attribute, which
is their distinguished name, and all other attributes are up to you to be defined.


## License

MIT (See [license file](LICENSE)).
