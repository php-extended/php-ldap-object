<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use RuntimeException;

/**
 * LdapException class file.
 * 
 * This exception does nothing special but helps differenciate with generic
 * exceptions given by other libraries.
 * 
 * @author Anastaszor
 */
class LdapException extends RuntimeException implements LdapThrowable
{
	
	// nothing to add
	
}
