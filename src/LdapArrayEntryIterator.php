<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;

/**
 * LdapArrayEntryIterator class file.
 *
 * This class is an array implementation of the LdapEntryIteratorInterface.
 *
 * @author Anastaszor
 * @extends \ArrayIterator<integer, LdapEntryInterface>
 */
class LdapArrayEntryIterator extends ArrayIterator implements LdapEntryIteratorInterface
{
	
	/**
	 * The count of the entries found by the query.
	 * 
	 * @var integer
	 */
	protected int $_queryCount;
	
	/**
	 * Builds a new LdapArrayEntryIterator with the given entries.
	 * 
	 * @param array<integer, LdapEntryInterface> $entries
	 * @param ?integer $queryCount
	 */
	public function __construct(array $entries = [], ?int $queryCount = null)
	{
		parent::__construct($entries);
		if(null === $queryCount)
		{
			$queryCount = $this->count();
		}
		$this->_queryCount = $queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === $this->count();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getFirstElement()
	 */
	public function getFirstElement() : ?LdapEntryInterface
	{
		foreach($this as $element)
		{
			return $element;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::mergeWith()
	 */
	public function mergeWith(LdapEntryIteratorInterface $result) : LdapEntryIteratorInterface
	{
		/** @var array<integer, LdapEntryInterface> $resultArray */
		$resultArray = [];
		
		/** @var LdapEntryInterface $object */
		foreach($this as $object)
		{
			$resultArray[] = $object;
		}
		
		/** @var LdapEntryInterface $object */
		foreach($result as $object)
		{
			$resultArray[] = $object;
		}
		
		return new self($resultArray, $this->getQueryCount() + $result->getQueryCount());
	}
	
}
