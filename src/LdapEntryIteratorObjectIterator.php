<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;

/**
 * LdapEntryIteratorObjectIterator interface file.
 * 
 * This implementation of a query result caches the entries and transforms
 * them into objects on the fly.
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 * @extends \ArrayIterator<integer, T>
 * @implements LdapObjectIteratorInterface<T>
 */
class LdapEntryIteratorObjectIterator extends ArrayIterator implements LdapObjectIteratorInterface
{
	
	/**
	 * The object factory.
	 * 
	 * @var LdapObjectFactoryInterface
	 */
	protected LdapObjectFactoryInterface $_objectFactory;
	
	/**
	 * The current known query count.
	 * 
	 * @var integer
	 */
	protected int $_queryCount = 0;
	
	/**
	 * Builds a new LdapEntryIteratorObjectIterator with its inner.
	 * 
	 * @param LdapObjectFactoryInterface $objectFactory
	 * @param array<integer, LdapEntryIteratorInterface> $entryIterators
	 * @param array<integer, LdapRecordInterface> $records
	 * @param integer $additionalQueryCount
	 * @psalm-param array<integer, T> $records
	 * @throws LdapThrowable
	 */
	public function __construct(LdapObjectFactoryInterface $objectFactory, array $entryIterators, array $records = [], int $additionalQueryCount = 0)
	{
		$this->_objectFactory = $objectFactory;
		$this->_queryCount = $additionalQueryCount;
		/** @var array<integer, T> $objects */
		$objects = [];
		
		foreach($entryIterators as $entryIterator)
		{
			$this->_queryCount += $entryIterator->getQueryCount();
			
			/** @var LdapEntryInterface $entry */
			foreach($entryIterator as $entry)
			{
				$objects[] = $objectFactory->buildObjectEntry($entry);
			}
		}
		
		foreach($records as $record)
		{
			$objects[] = $record;
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		parent::__construct($objects);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@('.((string) $this->getQueryCount()).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === $this->count();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::getFirstElement()
	 */
	public function getFirstElement() : ?LdapRecordInterface
	{
		foreach($this as $element)
		{
			return $element;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::mergeWith()
	 */
	public function mergeWith(LdapObjectIteratorInterface $result) : LdapObjectIteratorInterface
	{
		/** @var array<integer, T> $resultArray */
		$resultArray = [];
		
		/** @var T $object */
		foreach($this as $object)
		{
			$resultArray[] = $object;
		}
		
		/** @var T $object */
		foreach($result as $object)
		{
			$resultArray[] = $object;
		}
		
		return new LdapArrayObjectIterator($resultArray, $this->getQueryCount() + $result->getQueryCount());
	}
	
}
