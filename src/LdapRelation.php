<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\ParseThrowable;

/**
 * LdapRelation class file.
 * 
 * This class represents a relation between two ldap models.
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 * @implements LdapRelationInterface<T>
 */
class LdapRelation implements LdapRelationInterface
{
	
	/**
	 * The dn parser.
	 * 
	 * @var ?LdapDistinguishedNameParserInterface
	 */
	public static ?LdapDistinguishedNameParserInterface $dnParser = null;
	
	/**
	 * Gets the dn parser.
	 * 
	 * @return LdapDistinguishedNameParserInterface
	 */
	public static function getParser() : LdapDistinguishedNameParserInterface
	{
		if(null === self::$dnParser)
		{
			self::$dnParser = new LdapDistinguishedNameParser();
		}
		
		return self::$dnParser;
	}
	
	/**
	 * The type of relation.
	 * 
	 * @var string
	 */
	protected string $_typeOfRelation;
	
	/**
	 * The class of the source model.
	 * 
	 * @var class-string<LdapRecordInterface>
	 */
	protected string $_sourceClass;
	
	/**
	 * The key field of the source model.
	 * 
	 * @var string
	 */
	protected string $_sourceField;
	
	/**
	 * The class of the target model.
	 * 
	 * @var class-string<T>
	 */
	protected string $_targetClass;
	
	/**
	 * The key field of the target model.
	 * 
	 * @var string
	 */
	protected string $_targetField;
	
	/**
	 * Builds a new LdapRelation with the caracteristics of a given relation
	 * between two ldap models.
	 * 
	 * @param string $typeOfRelation
	 * @param class-string<LdapRecordInterface> $sourceClass
	 * @param string $sourceField
	 * @param class-string<T> $targetClass
	 * @param string $targetField
	 */
	public function __construct(string $typeOfRelation, string $sourceClass, string $sourceField, string $targetClass, string $targetField)
	{
		switch($typeOfRelation)
		{
			case self::MANY_TO_ONE:
			case self::ONE_TO_MANY:
			case self::ONE_TO_ONE:
				$this->_typeOfRelation = $typeOfRelation;
				break;
				
			default:
				$this->_typeOfRelation = self::MANY_TO_MANY;
				break;
		}
		
		$this->_sourceClass = $sourceClass;
		$this->_sourceField = $sourceField;
		$this->_targetClass = $targetClass;
		$this->_targetField = $targetField;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'('.$this->_typeOfRelation.')['.$this->_sourceClass.'|'.$this->_sourceField.' => '.$this->_targetClass.'|'.$this->_targetField.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRelationInterface::getTypeOfRelation()
	 */
	public function getTypeOfRelation() : string
	{
		return $this->_typeOfRelation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRelationInterface::getSourceClass()
	 */
	public function getSourceClass() : string
	{
		return $this->_sourceClass;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRelationInterface::getSourceField()
	 */
	public function getSourceField() : string
	{
		return $this->_sourceField;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRelationInterface::getTargetClass()
	 */
	public function getTargetClass() : string
	{
		return $this->_targetClass;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRelationInterface::getTargetField()
	 */
	public function getTargetField() : string
	{
		return $this->_targetField;
	}
	
	/**
	 * Searches for all records that are related to this object through the
	 * relation described by given relationname.
	 * 
	 * @param LdapRecordInterface $record
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<T>
	 * @throws LdapThrowable if the connection cannot be opened
	 * @throws LdapThrowable if the entry cannot be found in the given record
	 * @throws LdapThrowable if the objects cannot be built by the factory
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getRelatedObjects(LdapRecordInterface $record, ?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		// if the attribute of this model for the relation is the dn, then
		// we should give to the criteria the dn of this object
		if('dn' === $this->_sourceField)
		{
			$newcriteria->addValue($this->_targetField, $record->getDn()->getStringRepresentation());
			$targetclass = $this->_targetClass;
			
			return $targetclass::findAll($newcriteria);
		}
		
		$data = $record->__get($this->_sourceField);
		if(null === $data)
		{
			/** @var LdapArrayObjectIterator<T> $result */
			$result = new LdapArrayObjectIterator();
			
			return $result;
		}
		
		if(!\is_array($data))
		{
			$data = [$data];
		}
		
		if('dn' === $this->_targetField)
		{
			$dns = [];
			
			foreach($data as $strval)
			{
				if(\is_scalar($strval))
				{
					try
					{
						$dns[] = static::getParser()->parse((string) $strval);
					}
					// @codeCoverageIgnoreStart
					catch(ParseThrowable $exc)
					// @codeCoverageIgnoreEnd
					{
						// nothing to do
					}
				}
			}
			
			return $this->getRelatedObjectsByDn($dns, $newcriteria);
		}
		
		$newData = [];

		// conversion to int-key array
		foreach($data as $datum)
		{
			if(\is_object($datum))
			{
				$newData[] = \serialize($datum);
				continue;
			}
			if(\is_array($datum))
			{
				$innerData = [];
				
				foreach($datum as $datum2)
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					$innerData[] = (string) $datum2;
				}
				
				$newData[] = \implode(',', $innerData);
				continue;
			}
			$newData[] = $datum;
		}
		
		$newcriteria->addValues($this->_targetField, $newData);
		$targetclass = $this->_targetClass;
		
		return $targetclass::findAll($newcriteria);
	}
	
	/**
	 * Gets the related objects for the given dn data.
	 * 
	 * @param array<integer|string, LdapDistinguishedNameInterface> $dns
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function getRelatedObjectsByDn(array $dns, LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
	{
		// otherwise we have no choice than considering one relation after
		// another and then tie them together into one set
		// because filters based on (|(dn=uid=...)(dn=uid=...)) do not work
		// or will trigger administrative limit exceeded errors
		/** @var LdapArrayObjectIterator<T> $qres */
		$qres = new LdapArrayObjectIterator();
		$curCount = 0;
		$targetclass = $this->_targetClass;
		
		foreach($dns as $k => $thedn)
		{
			if($criteria->getOffset() <= $k && $criteria->getLimit() > $curCount)
			{
				/** @var ?T $result */
				$result = $targetclass::findByDn($thedn, $criteria);
				if(null !== $result)
				{
					/** @var LdapArrayObjectIterator<T> $nres */
					$nres = new LdapArrayObjectIterator([$result]);
					$qres = $qres->mergeWith($nres);
					$curCount++;
				}
			}
		}
		
		return $qres;
	}
	
}
