<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapNativeUserSession class file.
 * 
 * This class represents an user session that have no save mechanism.
 * Such user is only valid for the time of the current session.
 * 
 * @author Anastaszor
 */
class LdapMemoryUserSession implements LdapUserSessionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::create()
	 */
	public static function create(?string $username = null, ?string $password = null) : LdapUserSessionInterface
	{
		return new self($username, $password);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::load()
	 */
	public static function load() : ?LdapUserSessionInterface
	{
		return new self();
	}
	
	/**
	 * The username of the user.
	 * 
	 * @var ?string
	 */
	protected ?string $_username;
	
	/**
	 * The password of the user.
	 * 
	 * @var ?string
	 */
	protected ?string $_password;
	
	/**
	 * Builds a new LdapMemoryUserSession with the given username and password.
	 * 
	 * @param ?string $username
	 * @param ?string $password
	 */
	public function __construct(?string $username = null, ?string $password = null)
	{
		$this->_username = $username;
		$this->_password = $password;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return !$this->isAnonymous() ? (string) $this->_username : '(anonymous)';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::isAnonymous()
	 */
	public function isAnonymous() : bool
	{
		return (string) $this->_username === '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::getIdentifier()
	 */
	public function getIdentifier() : ?string
	{
		return $this->_username;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::getPassword()
	 */
	public function getPassword() : ?string
	{
		return $this->_password;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::save()
	 */
	public function save() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::logout()
	 */
	public function logout() : bool
	{
		$this->_username = null;
		$this->_password = null;
		
		return true;
	}
	
}
