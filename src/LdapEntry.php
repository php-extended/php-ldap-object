<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapEntry class file.
 * 
 * This class is a simple implementation of the LdapEntryInterface.
 * 
 * @author Anastaszor
 */
class LdapEntry implements LdapEntryInterface
{
	
	/**
	 * The dn of the entry.
	 * 
	 * @var LdapDistinguishedNameInterface
	 */
	protected LdapDistinguishedNameInterface $_dname;
	
	/**
	 * The objectClasses of the entry.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_objectClasses = [];
	
	/**
	 * The attributes of the entry.
	 * 
	 * @var array<string, int|float|string|array<integer, int|float|string>>
	 */
	protected array $_attributes = [];
	
	/**
	 * Builds a new LdapEntry with the given DN and attributes (objectClasses
	 * are given within the attributes array), like from the result of the
	 * ldap_get_attributes() function.
	 * 
	 * @param LdapDistinguishedNameInterface $dname
	 * @param array<integer|string, integer|float|string|array<integer|string, integer|float|string>> $attributes
	 */
	public function __construct(LdapDistinguishedNameInterface $dname, array $attributes)
	{
		$this->_dname = $dname;
		$this->_attributes = $this->processAttributes($attributes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_dname->__toString().' ['.\implode(', ', $this->_objectClasses).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::getDn()
	 */
	public function getDn() : LdapDistinguishedNameInterface
	{
		return $this->_dname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::hasObjectClass()
	 */
	public function hasObjectClass(string $objectClass) : bool
	{
		return \in_array($objectClass, $this->_objectClasses, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::getObjectClasses()
	 */
	public function getObjectClasses() : array
	{
		return $this->_objectClasses;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::hasAttribute()
	 */
	public function hasAttribute(string $attributeName) : bool
	{
		return isset($this->_attributes[$attributeName]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::getAttribute()
	 */
	public function getAttribute(string $attributeName)
	{
		if(isset($this->_attributes[$attributeName]))
		{
			return $this->_attributes[$attributeName];
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryInterface::getAttributes()
	 */
	public function getAttributes() : array
	{
		return $this->_attributes;
	}
	
	/**
	 * Processes the given attributes from the ldap attributes function. They
	 * come in the form of a multi dimensional array in the form of :.
	 *
	 * [
	 *   "sn" => [
	 *     "count" => 1,
	 *     0 => "<sn_value>",
	 *   ],
	 *
	 *   0 => "sn",
	 *
	 *   "cn" => [
	 *     "count" => 1,
	 *     0 => "<cn_value>",
	 *   ],
	 *
	 *   1 => "cn",
	 *
	 *   "objectClass" => [
	 *     "count" => 2,
	 *     0 => "top",
	 *     1 => "person",
	 *   ]
	 *
	 *   2 => "objectClass",
	 *
	 *   "count" => 3,
	 * ]
	 *
	 * @param array<integer|string, integer|float|string|array<integer|string, integer|float|string>> $attributes
	 * @return array<string, integer|float|string|array<integer, integer|float|string>>
	 */
	protected function processAttributes(array $attributes) : array
	{
		$returned = [];
		
		// ldap arrays contains one reserved var which is the count of
		// the size of the array, we can ignore it as we don't care
		unset($attributes['count']);
		
		foreach($attributes as $key => $values)
		{
			// ldap arrays are formed of numerical and string keys,
			// we can safely ignore numerical ones as they are redundant
			if(\is_int($key))
			{
				continue;
			}
			
			if(!\is_array($values))
			{
				$values = [$values];
			}
			
			unset($values['count']);
			
			if('objectclass' === \mb_strtolower($key))
			{
				$this->_objectClasses = [];
				
				foreach($values as $realv)
				{
					$this->_objectClasses[] = (string) $realv;
				}
				
				continue;
			}
			
			$attrvals = [];
			
			foreach($values as $realv)
			{
				$attrvals[] = $realv;
			}
			
			$returned[$key] = $attrvals;
		}
		
		return $returned;
	}
	
}
