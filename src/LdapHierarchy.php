<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;
use Iterator;

/**
 * LdapHierarchy class file.
 * 
 * This class is a simple implementation of the LdapHierarchyInterface.
 * 
 * @author Anastaszor
 */
class LdapHierarchy implements LdapHierarchyInterface
{
	
	/**
	 * All the levels, ordered from the higher level to the target level.
	 * 
	 * @var array<integer, LdapHierarchyLevel<LdapRecordInterface>>
	 */
	protected array $_levels = [];
	
	/**
	 * Builds a new LdapHierarchy from the given record and the upped bound
	 * of the hierarchy.
	 * 
	 * @param LdapRecordInterface $record
	 * @param LdapDistinguishedNameInterface $baseDn
	 */
	public function __construct(LdapRecordInterface $record, ?LdapDistinguishedNameInterface $baseDn = null)
	{
		$levels = [];
		$levels[] = new LdapHierarchyLevel($record);
		$parent = $record;
		
		while(null !== ($parent = $parent->getParent()))
		{
			$levels[] = new LdapHierarchyLevel($parent);
			if(null !== $baseDn && !$parent->getDn()->contains($baseDn))
			{
				break;
			}
		}
		
		$this->_levels = \array_reverse($levels);
		$levelCount = \count($this->_levels);
		
		foreach($this->_levels as $i => $level)
		{
			$level->setPositionAt($i + 1, $levelCount);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_levels);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyInterface::getShallow()
	 */
	public function getShallow() : LdapRecordInterface
	{
		// there is always at least one level
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		return $this->_levels[0]->getTarget();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyInterface::getTarget()
	 */
	public function getTarget() : LdapRecordInterface
	{
		// there is always at least one level
		return $this->_levels[\count($this->_levels) - 1]->getTarget();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyInterface::getHierarchyLevels()
	 */
	public function getHierarchyLevels() : Iterator
	{
		return new ArrayIterator($this->_levels);
	}
	
}
