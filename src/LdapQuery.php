<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;

/**
 * LdapQuery class file.
 *
 * This class an implementation of the LdapQueryInterface that takes the ldap
 * link structure and wraps it to get the ldap result.
 *
 * @author Anastaszor
 */
class LdapQuery implements LdapQueryInterface
{
	
	/**
	 * The ldap_link connection on which to execute the ldap_* functions.
	 *
	 * @var \LDAP\Connection
	 */
	protected $_ldapLink;
	
	/**
	 * The distinguished name parser.
	 *
	 * @var LdapDistinguishedNameParserInterface
	 */
	protected LdapDistinguishedNameParserInterface $_dnParser;
	
	/**
	 * The logger for all the events of this result set.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The base distinguished name that should be used when querying the ldap.
	 *
	 * @var LdapDistinguishedNameInterface
	 */
	protected LdapDistinguishedNameInterface $_baseDn;
	
	/**
	 * The full filter that should be used when querying the ldap.
	 * 
	 * @var LdapFilterNodeMultiInterface
	 */
	protected LdapFilterNodeMultiInterface $_filter;
	
	/**
	 * The scope to do the query.
	 * 
	 * @var integer
	 */
	protected int $_scope = LdapCriteriaInterface::LDAP_SCOPE_BASE;
	
	/**
	 * The offset from which this query must return results. Negative offsets
	 * are treated as zero.
	 * 
	 * @var integer
	 */
	protected int $_offset = 0;
	
	/**
	 * The number of records that this query must return.
	 * 
	 * @var integer
	 */
	protected int $_limit = 1000;
	
	/**
	 * Builds a new LdapQuery with its arguments.
	 * 
	 * @param \LDAP\Connection $ldapLink
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param LoggerInterface $logger
	 * @param LdapDistinguishedNameInterface $baseDn
	 * @param LdapFilterNodeMultiInterface $filter
	 * @param integer $scope
	 * @param integer $offset
	 * @param integer $limit
	 */
	public function __construct(
		\LDAP\Connection $ldapLink,
		LdapDistinguishedNameParserInterface $dnParser,
		LoggerInterface $logger,
		LdapDistinguishedNameInterface $baseDn,
		LdapFilterNodeMultiInterface $filter,
		int $scope = LdapCriteriaInterface::LDAP_SCOPE_BASE,
		int $offset = 0,
		int $limit = 1000
	) {
		$this->_ldapLink = $ldapLink;
		$this->_dnParser = $dnParser;
		$this->_logger = $logger;
		$this->_baseDn = $baseDn;
		$this->_filter = $filter;
		
		switch($scope)
		{
			case LdapCriteriaInterface::LDAP_SCOPE_BASE:
			case LdapCriteriaInterface::LDAP_SCOPE_ONE:
			case LdapCriteriaInterface::LDAP_SCOPE_SUBTREE:
				$this->_scope = $scope;
				break;
		}
		
		$this->_offset = \max(0, $offset);
		$this->_limit = \max(-1, $limit);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$str = '';
		
		switch($this->_scope)
		{
			case LdapCriteriaInterface::LDAP_SCOPE_BASE:
				$str .= 'READ';
				break;
				
			case LdapCriteriaInterface::LDAP_SCOPE_ONE:
				$str .= 'LIST';
				break;
				
			case LdapCriteriaInterface::LDAP_SCOPE_SUBTREE:
				$str .= 'TREE';
				break;
		}
		
		$str .= ' AT '.$this->_baseDn->__toString();
		$str .= ' FILTER '.$this->_filter->__toString();
		$str .= ' OFFSET '.((string) $this->_offset);
		$str .= ' LIMIT '.((string) $this->_limit);
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getBaseDn()
	 */
	public function getBaseDn() : LdapDistinguishedNameInterface
	{
		return $this->_baseDn;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getFilter()
	 */
	public function getFilter() : LdapFilterNodeMultiInterface
	{
		return $this->_filter;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getScope()
	 */
	public function getScope() : int
	{
		return $this->_scope;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getOffset()
	 */
	public function getOffset() : int
	{
		return $this->_offset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getLimit()
	 */
	public function getLimit() : int
	{
		return $this->_limit;
	}
	
	/**
	 * Processes the given callback between error handlers.
	 * 
	 * @param array<integer|string, boolean|integer|float|string> $context
	 * @param callable():(null|false|\LDAP\Result|array<integer|string, null|false|\LDAP\Result>) $callback
	 * @return ?\LDAP\Result
	 */
	public function processCallback(array $context, callable $callback) : ?\LDAP\Result
	{
		// silence warnings if the record does not exists
		$message = '{verb} at {dn} with filters {filters} OFFSET {offset} LIMIT {limit}';
		$this->_logger->info($message, $context);
		
		\set_error_handler(function()
		{ return false; });
		\error_clear_last();	// just in case
		$oldErrLvl = \error_reporting(\E_ALL ^ \E_WARNING);
		/** @var null|false|\LDAP\Result|array<integer|string, \LDAP\Result> $set */
		$set = $callback();
		\error_reporting($oldErrLvl); // put it back
		\restore_error_handler();
		
		if(\is_array($set))
		{
			$set = \array_shift($set);
		}
		
		if($set instanceof \LDAP\Result)
		{
			return $set;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryInterface::getResult()
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getResult() : LdapQueryResultInterface
	{
		$dnstr = $this->_baseDn->getStringRepresentation();
		$filterstr = $this->_filter->getStringRepresentation();
		
		// last chance : avoid error "-2 protocol error" for empty filter
		if(empty($filterstr))
		{
			$filterstr = '(objectClass=*)';
		}
		
		$context = [
			'dn' => $dnstr,
			'filters' => $filterstr,
			'offset' => $this->_offset,
			'limit' => $this->_limit,
		];
		$set = null;
		$queryCount = 0;
		
		switch($this->_scope)
		{
			case LdapCriteriaInterface::LDAP_SCOPE_BASE:
				$context['verb'] = 'READ';
				$set = $this->processCallback($context, function() use ($dnstr, $filterstr)
				{
					return \ldap_read($this->_ldapLink, $dnstr, $filterstr);
				});
				break;
				
			case LdapCriteriaInterface::LDAP_SCOPE_ONE:
				$context['verb'] = 'LIST';
				$set = $this->processCallback($context, function() use ($dnstr, $filterstr)
				{
					return \ldap_list($this->_ldapLink, $dnstr, $filterstr);
				});
				break;
				
			case LdapCriteriaInterface::LDAP_SCOPE_SUBTREE:
				$context['verb'] = 'TREE';
				$set = $this->processCallback($context, function() use ($dnstr, $filterstr)
				{
					return \ldap_search($this->_ldapLink, $dnstr, $filterstr);
				});
				break;
		}
		
		$errno = \ldap_errno($this->_ldapLink);
		
		$message = 'Execution result : [{status}] {count} entries ({errno} : {errmsg})';
		$context = [
			'status' => 'FAILED',
			'count' => $queryCount,
			'errno' => $errno,
			'errmsg' => \ldap_err2str($errno),
		];
		
		if(null !== $set)
		{
			// the query count is the number of entries in the set
			$queryCount = (int) \ldap_count_entries($this->_ldapLink, $set);
			$context['status'] = 'SUCCESS';
			$context['count'] = $queryCount;
		}
		
		$this->_logger->info($message, $context);
		
		return new LdapQueryResult(
			$this->_ldapLink,
			$set,
			$this->_dnParser,
			$this->_logger,
			$errno,
			$queryCount,
			$this->_offset,
			$this->_limit,
		);
	}
	
}
