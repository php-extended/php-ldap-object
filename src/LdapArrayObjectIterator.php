<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;

/**
 * LdapArrayObjectIterator class file.
 *
 * This class is an array implementation of the LdapObjectIteratorInterface.
 *
 * @author Anastaszor
 * @template T of LdapRecordInterface
 * @extends \ArrayIterator<integer, T>
 * @implements LdapObjectIteratorInterface<T>
 */
class LdapArrayObjectIterator extends ArrayIterator implements LdapObjectIteratorInterface
{
	
	/**
	 * The count of the entries found by the query.
	 *
	 * @var integer
	 */
	protected int $_queryCount;
	
	/**
	 * Builds a new LdapArrayObjectIterator with the given entries.
	 *
	 * @param array<integer, T> $entries
	 * @param ?integer $queryCount
	 */
	public function __construct(array $entries = [], ?int $queryCount = null)
	{
		parent::__construct($entries);
		if(null === $queryCount)
		{
			$queryCount = $this->count();
		}
		$this->_queryCount = $queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === $this->count();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::getFirstElement()
	 */
	public function getFirstElement() : ?LdapRecordInterface
	{
		foreach($this as $element)
		{
			return $element;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapObjectIteratorInterface::mergeWith()
	 */
	public function mergeWith(LdapObjectIteratorInterface $result) : LdapObjectIteratorInterface
	{
		/** @var array<integer, T> $resultArray */
		$resultArray = [];
		
		/** @var T $object */
		foreach($this as $object)
		{
			$resultArray[] = $object;
		}
		
		/** @var T $object */
		foreach($result as $object)
		{
			$resultArray[] = $object;
		}
		
		return new self($resultArray, $this->getQueryCount() + $result->getQueryCount());
	}
	
}
