<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\ParseThrowable;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Throwable;

/**
 * LdapQueryResult class file.
 * 
 * This class is an implementation of the LdapQueryResultInterface that wraps
 * the ldap result object and reifies entries on demand.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class LdapQueryResult implements LdapQueryResultInterface
{
	
	/**
	 * The ldap_link connection on which to execute the ldap_* functions.
	 *
	 * @var \LDAP\Connection
	 */
	protected $_ldapLink;
	
	/**
	 * The ldap_set that was executed, null if the set cannot be retrieved.
	 *
	 * @var null|\LDAP\Result
	 */
	protected $_ldapSet;
	
	/**
	 * The distinguished name parser.
	 *
	 * @var LdapDistinguishedNameParserInterface
	 */
	protected LdapDistinguishedNameParserInterface $_dnParser;
	
	/**
	 * The logger for all the events of this result set.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The error code of the result from the result set.
	 * 
	 * @var integer
	 */
	protected int $_errno = LdapConnectionInterface::LDAP_SUCCESS;
	
	/**
	 * The total number of records that the search result matches.
	 * 
	 * @var integer
	 */
	protected int $_queryCount = 0;
	
	/**
	 * The offset from which this query must return results. Negative offsets
	 * are treated as zero.
	 *
	 * @var integer
	 */
	protected int $_offset = 0;
	
	/**
	 * The number of records that this query must return.
	 *
	 * @var integer
	 */
	protected int $_limit = 1000;
	
	/**
	 * Builds a new LdapQueryResult from the data of an LdapQuery.
	 * 
	 * @param \LDAP\Connection $ldapLink
	 * @param ?\LDAP\Result $ldapSet
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param LoggerInterface $logger
	 * @param integer $errno
	 * @param integer $queryCount
	 * @param integer $offset
	 * @param integer $limit
	 */
	public function __construct(
		\LDAP\Connection $ldapLink,
		?\LDAP\Result $ldapSet,
		LdapDistinguishedNameParserInterface $dnParser,
		LoggerInterface $logger,
		int $errno = LdapConnectionInterface::LDAP_SUCCESS,
		int $queryCount = 0,
		int $offset = 0,
		int $limit = 1000
	) {
		$this->_ldapLink = $ldapLink;
		$this->_ldapSet = $ldapSet;
		$this->_dnParser = $dnParser;
		$this->_logger = $logger;
		$this->_errno = $errno;
		$this->_queryCount = \max(0, $queryCount);
		$this->_offset = \max(0, $offset);
		$this->_limit = \max(-1, $limit);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'RESULT : '.((string) $this->_queryCount)
			.' (OFFSET '.((string) $this->_offset).' LIMIT '.((string) $this->_limit).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryResultInterface::hasError()
	 */
	public function hasError() : bool
	{
		if(LdapConnectionInterface::LDAP_SUCCESS === $this->_errno)
		{
			return false;
		}
		
		// no such object is not an error, it means that the object at
		// the baseDn is not found. In any case, it means the count of
		// available objects is zero for read, find and search and the
		// iterator iterates over an empty set.
		if(LdapConnectionInterface::LDAP_NO_SUCH_OBJECT === $this->_errno)
		{
			return false;
		}
		
		// if the error is adminlimit exceeded, the returned set is only
		// partial results, which is fine
		return !(LdapConnectionInterface::LDAP_ADMINLIMIT_EXCEEDED === $this->_errno);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryResultInterface::getError()
	 */
	public function getError() : int
	{
		return $this->_errno;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		/** @phpstan-ignore-next-line */
		return \max(0, $this->_limit, $this->getQueryCount() - $this->_offset);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryResultInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapQueryResultInterface::getEntries()
	 * @throws RuntimeException
	 */
	public function getEntries() : LdapEntryIteratorInterface
	{
		// if the result set is not available, get an empty iterator
		if(null === $this->_ldapSet)
		{
			return new LdapArrayEntryIterator([], $this->_queryCount);
		}
		
		// if the offset is above the current count, get an empty iterator
		if($this->_offset > $this->_queryCount)
		{
			return new LdapArrayEntryIterator([], $this->_queryCount);
		}
		
		// if no records are asked, get an empty iterator
		if(0 === $this->_limit)
		{
			return new LdapArrayEntryIterator([], $this->_queryCount);
		}
		
		$ldapEntry = $this->skipToFirstLdapEntry();
		
		return $this->buildEntryIterator($ldapEntry);
	}
	
	/**
	 * Gets the next entry according to the previous status.
	 * 
	 * @param integer $idx
	 * @param null|\LDAP\ResultEntry $prevEntry
	 * @return \LDAP\ResultEntry
	 * @throws RuntimeException
	 */
	protected function getNextLdapEntry(int $idx, ?\LDAP\ResultEntry $prevEntry)
	{
		if(null === $this->_ldapSet)
		{
			$message = 'Failed to get entry : no result set available';
			
			throw new RuntimeException($message);
		}
		
		if(null === $prevEntry)
		{
			/** @var false|\LDAP\ResultEntry $result */
			$result = \ldap_first_entry($this->_ldapLink, $this->_ldapSet);
			if(false === $result)
			{
				$errno = \ldap_errno($this->_ldapLink);
				$message = 'Failed to get the entry nb {k} before offset {offset} : {errno} {errmsg}';
				$context = [
					'{k}' => $idx,
					'{offset}' => $this->_offset,
					'{errno}' => $errno,
					'{errmsg}' => \ldap_err2str($errno),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			return $result;
		}
		
		/** @var false|\LDAP\ResultEntry $result */
		$result = \ldap_next_entry($this->_ldapLink, $prevEntry);
		if(false === $result)
		{
			$errno = \ldap_errno($this->_ldapLink);
			$message = 'Failed to get the entry nb {k} after offset {offset} before limit {limit} : {errno} {errmsg}';
			$context = [
				'{k}' => $idx,
				'{offset}' => $this->_offset,
				'{limit}' => $this->_limit,
				'{errno}' => $errno,
				'{errmsg}' => \ldap_err2str($errno),
			];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $result;
	}
	
	/**
	 * Gets the distinguished name for the given current entry.
	 * 
	 * @param \LDAP\ResultEntry $ldapEntry
	 * @return LdapDistinguishedNameInterface
	 * @throws RuntimeException
	 */
	protected function getLdapDn(\LDAP\ResultEntry $ldapEntry) : LdapDistinguishedNameInterface
	{
		$ldn = \ldap_get_dn($this->_ldapLink, $ldapEntry);
		if(false !== $ldn)
		{
			try
			{
				return $this->_dnParser->parse($ldn);
			}
			catch(ParseThrowable $e)
			{
				$message = 'Failed to parse DN from entry {dn}';
				$context = ['{dn}' => $ldn];
				
				throw new RuntimeException(\strtr($message, $context), -1, $e);
			}
		}
		
		$errno = \ldap_errno($this->_ldapLink);
		$message = 'Failed to get dn from entry : {errno} {errmsg}';
		$context = ['{errno}' => $errno, '{errmsg}' => \ldap_err2str($errno)];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Gets the ldap attributes from the ldap functions.
	 * 
	 * @param \LDAP\ResultEntry $ldapEntry
	 * @return array<integer|string, integer|float|string|array<integer|string, integer|float|string>>
	 * @throws RuntimeException
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	protected function getLdapAttributes(\LDAP\ResultEntry $ldapEntry) : array
	{
		return \ldap_get_attributes($this->_ldapLink, $ldapEntry);
	}
	
	/**
	 * Logs the given exception.
	 * 
	 * @param Throwable $exc
	 */
	protected function logException(Throwable $exc) : void
	{
		$ctr = 0;
		
		do
		{
			$this->_logger->error($exc->getMessage());
			$this->_logger->error($exc->getTraceAsString());
			$exc = $exc->getPrevious();
			$ctr++;
		}
		while(null !== $exc && 10 > $ctr);
	}
	
	/**
	 * Gets the previous-first entry that is eligible to be reified regarding
	 * the offset count.
	 * 
	 * @return ?\LDAP\ResultEntry
	 * @throws RuntimeException
	 */
	protected function skipToFirstLdapEntry() : ?\LDAP\ResultEntry
	{
		// first, jump to the offset-th record in the set
		$ldapEntry = null;
		
		for($cpt = 0; $cpt < $this->_offset; $cpt++)
		{
			$ldapEntry = $this->getNextLdapEntry($cpt, $ldapEntry);
		}
		
		return $ldapEntry;
	}
	
	/**
	 * Builds an entry object from the entry.
	 * 
	 * @param ?\LDAP\ResultEntry $ldapEntry
	 * @return LdapEntryIteratorInterface
	 * @throws RuntimeException
	 */
	protected function buildEntryIterator(?\LDAP\ResultEntry $ldapEntry) : LdapEntryIteratorInterface
	{
		// next, fetch the other remaining ones until the limit is reached
		// if limit is negative then the limit is the count of the query
		$realLimit = \max(0, $this->_queryCount - $this->_offset);
		if(0 < $this->_limit)
		{
			$realLimit = \min($this->_limit, $realLimit);
		}
		
		$entries = [];
		
		for($cpt = 0; $cpt < $realLimit; $cpt++)
		{
			$ldapEntry = $this->getNextLdapEntry($this->_offset + $cpt, $ldapEntry);
			
			try
			{
				$entries[] = new LdapEntry($this->getLdapDn($ldapEntry), $this->getLdapAttributes($ldapEntry));
			}
			catch(RuntimeException $exc)
			{
				$this->logException($exc);
			}
		}
		
		return new LdapArrayEntryIterator($entries, $this->_queryCount);
	}
	
}
