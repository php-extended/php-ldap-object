<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;

/**
 * LdapRecursiveCachedEntryIterator class file.
 * 
 * This class reassembles the LdapEntryIteratorInterface, LdapQueryFetchInterface
 * and LdapResultSetInterface without having to use the query fetch and the
 * result set structures.
 * 
 * @author Anastaszor
 */
class LdapRecursiveCachedEntryIterator extends LdapMultiCachedEntryIterator implements LdapEntryIteratorInterface
{
	
	/**
	 * Builds a new LdapRecursiveCachedEntryIterator with the given factory, 
	 * ldap link, baseDn, criteria, logger and recursive string.
	 * 
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param \LDAP\Connection $ldapLink
	 * @param LdapDistinguishedNameInterface $baseDn
	 * @param LdapCriteriaInterface $criteria
	 * @param LoggerInterface $logger
	 * @param string $previousValue the previous value for the field on which
	 *                              the recursive search takes place
	 * @throws LdapThrowable if the connection cannot be opened
	 * @throws LdapThrowable if the objects cannot be built by the factory
	 */
	public function __construct(LdapDistinguishedNameParserInterface $dnParser, \LDAP\Connection $ldapLink, LdapDistinguishedNameInterface $baseDn, LdapCriteriaInterface $criteria, LoggerInterface $logger, ?string $previousValue = null)
	{
		// we clone the criteria to avoid side effects
		$innerCrit = clone $criteria;
		
		$recordClass = $innerCrit->getRecordClass();
		// cn : wild guess, may not work
		$idField = null === $recordClass ? 'cn' : $recordClass::getIdentifierField();
		
		if(null !== $previousValue)
		{
			// we add an additional filter on a unique field that looks like :
			// (field=[letter]*) ; then (field=[letter][letter]*) ; and so on
			// if the criteria already has a known filter on the given field
			// then replace the existing values
			$innerCrit->removeFiltersFor($idField);
			$innerCrit->addValue($idField, $previousValue.'*');
		}
		
		$query = new LdapQuery(
			$ldapLink,
			$dnParser,
			$logger,
			$baseDn,
			$innerCrit->getAllFilters(),
			$innerCrit->getScope(),
			$innerCrit->getOffset(),
			$innerCrit->getLimit(),
		);
		$result = $query->getResult();
		
		if(!$result->hasError())
		{
			parent::__construct([$result->getEntries()]);
			
			return;
		}
		
		if(LdapConnectionInterface::LDAP_NO_SUCH_OBJECT === $result->getError())
		{
			// nothing to do, just returns an empty set
			return;
		}
		
		if(LdapConnectionInterface::LDAP_INVALID_SYNTAX === $result->getError())
		{
			// should not happen
			$message = 'Bad search filter : {filter}';
			$context = ['{filter}' => $criteria->getAllFilters()->__toString()];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		if(\in_array($result->getError(), [
			LdapConnectionInterface::LDAP_ADMINLIMIT_EXCEEDED,
			LdapConnectionInterface::LDAP_SIZELIMIT_EXCEEDED,
		], true))
		{
			$results = $this->processRecursive($dnParser, $ldapLink, $baseDn, $innerCrit, $logger, $idField, $previousValue ?? '');
			
			parent::__construct($results);
			
			return;
		}
		
		$message = 'Unknown error at {baseDn} with {filter} : {error} {errmsg}';
		$context = [
			'{baseDn}' => $baseDn->__toString(),
			'{filter}' => $criteria->getAllFilters()->__toString(),
			'{error}' => $result->getError(),
			'{errmsg}' => \ldap_err2str($result->getError()),
		];
		
		throw new LdapException(\strtr($message, $context));
	}
	
	/**
	 * Processes the recurive part of this result.
	 * 
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param \LDAP\Connection $ldapLink
	 * @param LdapDistinguishedNameInterface $baseDn
	 * @param LdapCriteriaInterface $innerCrit
	 * @param LoggerInterface $logger
	 * @param string $idField
	 * @param string $previousValue
	 * @return array<integer, LdapEntryIteratorInterface>
	 */
	public function processRecursive(LdapDistinguishedNameParserInterface $dnParser, \LDAP\Connection $ldapLink, LdapDistinguishedNameInterface $baseDn, LdapCriteriaInterface $innerCrit, LoggerInterface $logger, string $idField, ?string $previousValue = null) : array
	{
		
		$currentOffset = $innerCrit->getOffset();
		$currentLimit = $innerCrit->getLimit();
		$currentFilters = $innerCrit->getAllFilters();
		$includeDigits = 'uid' === $idField || 'ou' === $idField;
		$entries = [];
		
		foreach($this->generateCharArray($previousValue, $includeDigits) as $newChar)
		{
			$newWord = ((string) $previousValue).$newChar;
			
			$query = new LdapQuery(
				$ldapLink,
				$dnParser,
				$logger,
				$baseDn,
				$currentFilters->removeNodesFor($idField)->addAndValues($idField, [$newWord.'*']),
				$innerCrit->getScope(),
				$currentOffset,
				$currentLimit,
			);
			$result = $query->getResult();
			
			$queryCount = 0;
			
			if(!$result->hasError())
			{
				$entryIterator = $result->getEntries();
				
				$entries[] = $entryIterator;
				
				$queryCount = $entryIterator->getQueryCount();
			}
			
			if($currentOffset > $queryCount)
			{
				$currentOffset -= $queryCount;
				continue;
			}
			
			$queryCount -= $currentOffset;
			$currentOffset = 0;
			$currentLimit = \max(0, $currentLimit - $queryCount);
		}
		
		return $entries;
	}
	
	/**
	 * Gets a char array with all characters that are allowed in fields for
	 * searching by by recursion in case the pagination is not available.
	 *
	 * @param string $previousLetter
	 * @param boolean $includeDigits
	 * @return array<integer, string>
	 */
	public function generateCharArray(?string $previousLetter, bool $includeDigits) : array
	{
		$previousLetter = (string) \mb_substr((string) $previousLetter, -1);
		
		$chars = [];
		if(!\in_array($previousLetter, ['', '-', ' ', '"', '/'], true))
		{
			$chars = ['-', ' ', '"', '/'];
		}
		
		for($i = \ord('a'); \ord('z') >= $i; $i++)
		{
			$chars[] = \chr($i);
		}
		
		if($includeDigits)
		{
			for($i = \ord('0'); \ord('9') >= $i; $i++)
			{
				$chars[] = \chr($i);
			}
		}
		
		return $chars;
	}
	
}
