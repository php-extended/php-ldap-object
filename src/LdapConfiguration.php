<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use InvalidArgumentException;

/**
 * LdapConfiguration class file.
 * 
 * This class is a simple implementation of the LdapConfigurationInterface.
 * 
 * @author Anastaszor
 */
class LdapConfiguration implements LdapConfigurationInterface
{
	
	/**
	 * The hostname on which to connect, or the IP address of the ldap server.
	 * 
	 * @var string
	 */
	protected string $_hostname;
	
	/**
	 * The TCP or UDP port for the connexion.
	 * 
	 * @var integer
	 */
	protected int $_port = 389;
	
	/**
	 * Whether to use a secure connection (ldaps).
	 * 
	 * @var boolean
	 */
	protected bool $_secure = true;
	
	/**
	 * The maximum time to wait for the connexion to be established, in seconds
	 * (0 = no limit). This does not work with php < 5.3.
	 * 
	 * @var integer
	 */
	protected int $_connectionTimeout = 10;
	
	/**
	 * The suffix dn in which all the records are in the hierarchy. Searches
	 * cannot go higher in the tree than this suffix dn.
	 * 
	 * @var LdapDistinguishedNameInterface
	 */
	protected LdapDistinguishedNameInterface $_suffixDn;
	
	/**
	 * The default username (dn) that will be used for authentication. If null,
	 * it means anonymous login.
	 * 
	 * @var ?string
	 */
	protected ?string $_login = null;
	
	/**
	 * The default password that will be used for authentication. If null,
	 * it means anonymous login.
	 * 
	 * @var ?string
	 */
	protected ?string $_password = null;
	
	/**
	 * The ldap client version (from 1 to 3).
	 * 
	 * @var integer
	 */
	protected int $_version = 3;
	
	/**
	 * Whether the client should follow referrals automatically.
	 * 
	 * @var boolean
	 */
	protected bool $_followsReferral = false;
	
	/**
	 * The dereference policy, that specifies how aliases are dereferenced.
	 * (0 = NEVER ; 1 = SEARCHING ; 2 = FINDING ; 3 = ALWAYS).
	 * 
	 * @var integer
	 */
	protected int $_dereferencePolicy = 0;
	
	/**
	 * The size limit of any ldap query result (number of objects). 0 is no
	 * limit.
	 * 
	 * @var integer
	 */
	protected int $_sizeLimit = 0;
	
	/**
	 * The time limit of any ldap operation (milliseconds). 0 is no limit.
	 * 
	 * @var integer
	 */
	protected int $_timeLimit = 0;
	
	/**
	 * Whether to ping the ldap server before connecting to it.
	 * 
	 * @var boolean
	 */
	protected bool $_servicepingEnabled = true;
	
	/**
	 * Builds a new LdapConfiguration with the given hostname and suffix Dn.
	 * 
	 * @param string $hostname
	 * @param LdapDistinguishedNameInterface $suffixDn
	 */
	public function __construct(string $hostname, LdapDistinguishedNameInterface $suffixDn)
	{
		$this->setHostname($hostname);
		$this->setSuffixDn($suffixDn);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$str = ($this->_secure ? 'ldaps://' : 'ldap://');
		$str .= ($this->_login ?? '(anonymous)').'@';
		
		return $str.$this->_hostname.':'.((string) $this->_port);
	}
	
	/**
	 * Sets the hostname for this configuration.
	 * 
	 * @param string $hostname
	 * @return LdapConfigurationInterface
	 */
	public function setHostname(string $hostname) : LdapConfigurationInterface
	{
		$this->_hostname = $hostname;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getHostname()
	 */
	public function getHostname() : string
	{
		return $this->_hostname;
	}
	
	/**
	 * Sets the port for this configuration.
	 * 
	 * @param integer $port
	 * @return LdapConfigurationInterface
	 */
	public function setPort(int $port) : LdapConfigurationInterface
	{
		$this->_port = $port;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getPort()
	 */
	public function getPort() : int
	{
		return $this->_port;
	}
	
	/**
	 * Sets whether this configuration should use a secure connection.
	 * 
	 * @param boolean $secure
	 * @return LdapConfigurationInterface
	 */
	public function setSecure(bool $secure) : LdapConfigurationInterface
	{
		$this->_secure = $secure;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::isSecure()
	 */
	public function isSecure() : bool
	{
		return $this->_secure;
	}
	
	/**
	 * Sets the connection timeout of this configuration.
	 * 
	 * @param integer $connectionTimeout
	 * @return LdapConfigurationInterface
	 */
	public function setConnectionTimeout(int $connectionTimeout) : LdapConfigurationInterface
	{
		$this->_connectionTimeout = $connectionTimeout;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getConnectionTimeout()
	 */
	public function getConnectionTimeout() : int
	{
		return $this->_connectionTimeout;
	}
	
	/**
	 * Sets the suffix distinguished name for this configuration.
	 * 
	 * @param LdapDistinguishedNameInterface $suffixDn
	 * @return LdapConfigurationInterface
	 */
	public function setSuffixDn(LdapDistinguishedNameInterface $suffixDn) : LdapConfigurationInterface
	{
		$this->_suffixDn = $suffixDn;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getSuffixDn()
	 */
	public function getSuffixDn() : LdapDistinguishedNameInterface
	{
		return $this->_suffixDn;
	}
	
	/**
	 * Sets the login for this configuration.
	 * 
	 * @param string $login
	 * @return LdapConfigurationInterface
	 */
	public function setLogin(string $login) : LdapConfigurationInterface
	{
		$this->_login = $login;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getLogin()
	 */
	public function getLogin() : ?string
	{
		return $this->_login;
	}
	
	/**
	 * Sets the password for this configuration.
	 * 
	 * @param string $password
	 * @return LdapConfigurationInterface
	 */
	public function setPassword(string $password) : LdapConfigurationInterface
	{
		$this->_password = $password;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getPassword()
	 */
	public function getPassword() : ?string
	{
		return $this->_password;
	}
	
	/**
	 * Sets the version number for this configuration.
	 * 
	 * @param integer $version
	 * @return LdapConfigurationInterface
	 * @throws InvalidArgumentException if the version is not an integer not
	 *                                  null between 1 and 3
	 */
	public function setVersion(int $version) : LdapConfigurationInterface
	{
		$this->_version = $version;
		if(1 > $this->_version || 3 < $this->_version)
		{
			$this->_version = 3;	// set default
			
			throw new InvalidArgumentException('The version must be included between 1 and 3.');
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getVersion()
	 */
	public function getVersion() : int
	{
		return $this->_version;
	}
	
	/**
	 * Sets whether this configuration must follow referrals.
	 * 
	 * @param boolean $followsReferral
	 * @return LdapConfigurationInterface
	 */
	public function setFollowsReferral(bool $followsReferral) : LdapConfigurationInterface
	{
		$this->_followsReferral = $followsReferral;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::followsReferral()
	 */
	public function followsReferral() : bool
	{
		return $this->_followsReferral;
	}
	
	/**
	 * Sets the dereference policy for this configuration.
	 * 
	 * @param integer $deferencePolicy
	 * @return LdapConfigurationInterface
	 * @throws InvalidArgumentException if the dereferencePolicy is not an
	 *                                  integer between 0 and 3
	 */
	public function setDereferencePolicy(int $deferencePolicy) : LdapConfigurationInterface
	{
		$this->_dereferencePolicy = $deferencePolicy;
		if(0 > $this->_dereferencePolicy || 3 < $this->_dereferencePolicy)
		{
			$this->_dereferencePolicy = 0;	// set default
			
			throw new InvalidArgumentException('The dereferencePolicy must be included between 0 and 3.');
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getDereferencePolicy()
	 */
	public function getDereferencePolicy() : int
	{
		return $this->_dereferencePolicy;
	}
	
	/**
	 * Sets the size limit for this configuration.
	 * 
	 * @param integer $sizeLimit
	 * @return LdapConfigurationInterface
	 * @throws InvalidArgumentException if the size limit is not positive
	 */
	public function setSizeLimit(int $sizeLimit) : LdapConfigurationInterface
	{
		$this->_sizeLimit = $sizeLimit;
		if(0 > $this->_sizeLimit)
		{
			$this->_sizeLimit = 0;
			
			throw new InvalidArgumentException('The size limit must be a positive integer.');
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getSizeLimit()
	 */
	public function getSizeLimit() : int
	{
		return $this->_sizeLimit;
	}
	
	/**
	 * Sets the time limit for this configuration.
	 * 
	 * @param integer $timeLimit
	 * @return LdapConfigurationInterface
	 * @throws InvalidArgumentException if the time limit is not positive
	 */
	public function setTimeLimit(int $timeLimit) : LdapConfigurationInterface
	{
		$this->_timeLimit = $timeLimit;
		if(0 > $this->_timeLimit)
		{
			$this->_timeLimit = 0;
			
			throw new InvalidArgumentException('The time limit must be a positive integer.');
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::getTimeLimit()
	 */
	public function getTimeLimit() : int
	{
		return $this->_timeLimit;
	}
	
	/**
	 * Sets whether to enable the service ping before connecting to the ldap.
	 * 
	 * @param boolean $servicePingEnabled
	 * @return LdapConfigurationInterface
	 */
	public function setServicePingEnabled(bool $servicePingEnabled) : LdapConfigurationInterface
	{
		$this->_servicepingEnabled = $servicePingEnabled;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConfigurationInterface::isServicePingEnabled()
	 */
	public function isServicePingEnabled() : bool
	{
		return $this->_servicepingEnabled;
	}
	
}
