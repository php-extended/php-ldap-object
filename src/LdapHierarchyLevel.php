<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapHierarchyLevel class file.
 * 
 * This class is a simple implementation of the LdapHierarchyLevelInterface.
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 * @implements LdapHierarchyLevelInterface<T>
 */
class LdapHierarchyLevel implements LdapHierarchyLevelInterface
{
	
	/**
	 * The target for this level of the hierarchy.
	 * 
	 * @var T
	 */
	protected LdapRecordInterface $_target;
	
	/**
	 * The current position of this level in the hierarchy.
	 * 
	 * @var integer
	 */
	protected int $_position = 1;
	
	/**
	 * The maximum positions of this hierarchy.
	 * 
	 * @var integer
	 */
	protected int $_max = 1;
	
	/**
	 * Builds a new LdapHierarchyLevel with the given target record.
	 * 
	 * @param T $record
	 */
	public function __construct(LdapRecordInterface $record)
	{
		$this->_target = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@level['.((string) $this->_position).'/'.((string) $this->_max).']';
	}
	
	/**
	 * Sets the current position information of this level in the hierarchy.
	 * 
	 * @param integer $position
	 * @param integer $maxPosition
	 */
	public function setPositionAt(int $position, int $maxPosition) : void
	{
		$this->_position = $position;
		$this->_max = $maxPosition;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyLevelInterface::getTarget()
	 */
	public function getTarget() : LdapRecordInterface
	{
		return $this->_target;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyLevelInterface::isFirst()
	 */
	public function isFirst() : bool
	{
		return 1 === $this->_position;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyLevelInterface::isLast()
	 */
	public function isLast() : bool
	{
		return $this->_position === $this->_max;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyLevelInterface::getLevel()
	 */
	public function getLevel() : int
	{
		return $this->_position;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapHierarchyLevelInterface::getNeighbors()
	 * @psalm-suppress InvalidReturnType
	 */
	public function getNeighbors(?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface
	{
		if(null === $criteria)
		{
			$criteria = new LdapCriteria();
		}
		
		if(null === $criteria->getRecordClass())
		{
			$criteria->setRecordClass(\get_class($this->_target));
		}
		
		$parent = $this->_target->getParent();
		if(null === $parent)
		{
			return new LdapArrayObjectIterator();
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $parent->findChildren($criteria);
	}
	
}
