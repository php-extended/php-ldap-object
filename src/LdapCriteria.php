<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapCriteria class file.
 * 
 * This class is a simple implementation of the LdapCriteriaInterface.
 * 
 * @author Anastaszor
 */
class LdapCriteria implements LdapCriteriaInterface
{
	
	/**
	 * The number of records to skip before fetching. Zero or negative is
	 * no offset.
	 * 
	 * @var integer
	 */
	protected int $_offset = 0;
	
	/**
	 * The maximum number of records to fetch. Zero is no records, -1 is
	 * unlimited.
	 * 
	 * @var integer
	 */
	protected int $_limit = self::DEFAULT_LIMIT;
	
	/**
	 * The model to search objects for.
	 * 
	 * @var ?class-string<LdapRecordInterface>
	 */
	protected ?string $_recordClass = null;
	
	/**
	 * The base dns to search for the subtrees.
	 * 
	 * @var array<integer, LdapDistinguishedNameInterface>
	 */
	protected array $_baseDns = [];
	
	/**
	 * The scope of the search.
	 * 
	 * @var integer
	 */
	protected int $_scope = self::LDAP_SCOPE_BASE;
	
	/**
	 * The root node of the filter.
	 * 
	 * @var ?LdapFilterNodeMultiInterface
	 */
	protected ?LdapFilterNodeMultiInterface $_filter = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$strbase = '(empty)';
		$bases = [];
		
		foreach($this->_baseDns as $base)
		{
			$bases[] = $base->getStringRepresentation();
		}
		
		if(0 < \count($bases))
		{
			$strbase = "[\n\t".\implode("\n\t", $bases)."\n]";
		}
		
		$str = __CLASS__."\n";
		$str .= 'BASES '.$strbase."\n";
		$str .= 'RECORD '.($this->_recordClass ?? 'NULL')."\n";
		$str .= 'OFFSET '.((string) $this->_offset)."\n";
		$str .= 'LIMIT '.((string) $this->_limit)."\n";
		$str .= 'SCOPE '.((string) $this->_scope).' ('.(LdapCriteria::LDAP_SCOPE_BASE === $this->_scope ? 'BASE' : (LdapCriteria::LDAP_SCOPE_ONE === $this->_scope ? 'LIST' : 'TREE')).")\n";
		$str .= 'FILTER '.(null === $this->_filter ? '(empty)' : $this->_filter->getStringRepresentation());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		if(!empty($this->_baseDns))
		{
			return false;
		}
		
		if(!empty($this->_recordClass))
		{
			return false;
		}
		
		return null === $this->_filter || $this->_filter->isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setOffset()
	 */
	public function setOffset(int $offset) : LdapCriteriaInterface
	{
		$this->_offset = \max(0, $offset);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getOffset()
	 */
	public function getOffset() : int
	{
		return $this->_offset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setLimit()
	 */
	public function setLimit(int $limit) : LdapCriteriaInterface
	{
		if(0 > $limit)
		{
			$limit = self::DEFAULT_LIMIT;
		}
		
		$this->_limit = $limit;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getLimit()
	 */
	public function getLimit() : int
	{
		return $this->_limit;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setScope()
	 */
	public function setScope(int $scope) : LdapCriteriaInterface
	{
		$this->_scope = \min(LdapCriteria::LDAP_SCOPE_SUBTREE, \max(LdapCriteria::LDAP_SCOPE_BASE, $scope));
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getScope()
	 */
	public function getScope() : int
	{
		return $this->_scope;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setRecordClass()
	 */
	public function setRecordClass(string $recordClass) : LdapCriteriaInterface
	{
		$this->_recordClass = $recordClass;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getRecordClass()
	 */
	public function getRecordClass() : ?string
	{
		return $this->_recordClass;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addBaseDn()
	 */
	public function addBaseDn(LdapDistinguishedNameInterface $baseDn) : LdapCriteriaInterface
	{
		if($baseDn->isEmpty())
		{
			return $this;
		}
		
		foreach($this->_baseDns as $currentDn)
		{
			// if current dn is more broad than the base dn
			// then ignore the more precise base dn
			if($currentDn->contains($baseDn))
			{
				return $this;
			}
		}
		
		$unset = false;
		
		foreach($this->_baseDns as $k => $currentDn)
		{
			// if the base dn is more broad than the current dn
			// then remove the more precise current dn
			if($baseDn->contains($currentDn))
			{
				unset($this->_baseDns[$k]);
				$unset = true;
			}
		}
		
		if($unset)
		{
			$this->_baseDns = \array_values($this->_baseDns);
		}
		
		$this->_baseDns[] = $baseDn;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setBaseDn()
	 */
	public function setBaseDn(LdapDistinguishedNameInterface $baseDn) : LdapCriteriaInterface
	{
		$this->_baseDns = [];
		
		return $this->addBaseDn($baseDn);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addAllBaseDns()
	 */
	public function addAllBaseDns(array $baseDns) : LdapCriteriaInterface
	{
		foreach($baseDns as $baseDn)
		{
			$this->addBaseDn($baseDn);
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setAllBaseDns()
	 */
	public function setAllBaseDns(array $baseDns) : LdapCriteriaInterface
	{
		$this->_baseDns = [];
		
		return $this->addAllBaseDns($baseDns);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getBaseDns()
	 */
	public function getBaseDns() : array
	{
		return $this->_baseDns;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::setFilters()
	 */
	public function setFilters(LdapFilterNodeMultiInterface $rootNode) : LdapCriteriaInterface
	{
		$this->_filter = $rootNode;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addFilterNode()
	 */
	public function addFilterNode(LdapFilterNodeInterface $node) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addNode($node);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addFilterNodes()
	 */
	public function addFilterNodes(array $nodes) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addNodes($nodes);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addValue()
	 */
	public function addValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addValue($column, $value, $comparator);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addValue()
	 */
	public function addNotValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addNotValue($column, $value, $comparator);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addValues()
	 */
	public function addValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addOrValues($column, $values, $comparator);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::addNotValues()
	 */
	public function addNotValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->addNorValues($column, $values, $comparator);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::removeFiltersFor()
	 */
	public function removeFiltersFor(string $column) : LdapCriteriaInterface
	{
		$this->_filter = $this->getFilters()->removeNodesFor($column);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getFilters()
	 */
	public function getFilters() : LdapFilterNodeMultiInterface
	{
		if(null === $this->_filter)
		{
			$this->_filter = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		}
		
		return $this->_filter;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::getAllFilters()
	 */
	public function getAllFilters() : LdapFilterNodeMultiInterface
	{
		if(null === $this->_recordClass)
		{
			return $this->getFilters();
		}
		
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addNode($this->getFilters());
		$node->addValue('objectClass', $this->_recordClass::getLdapObjectClass());
		
		foreach($this->_recordClass::getLdapClassCriteriaFilters() as $filter)
		{
			$node->addNode($filter);
		}
		
		return $node;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapCriteriaInterface::mergeWith()
	 */
	public function mergeWith(LdapCriteriaInterface $criteria) : LdapCriteriaInterface
	{
		$new = new self();
		$new->setOffset(\max([$this->getOffset(), $criteria->getOffset()]));
		$new->setLimit(\max([$this->getLimit(), $criteria->getLimit()]));
		
		$recordClass = $this->getRecordClass();
		if(null !== $recordClass)
		{
			$new->setRecordClass($recordClass);
		}
		
		$recordClass = $criteria->getRecordClass();
		if(null !== $recordClass)
		{
			$new->setRecordClass($recordClass);
		}
		
		$new->setScope(\max($this->getScope(), $criteria->getScope()));
		
		$new->addAllBaseDns($this->getBaseDns());
		$new->addAllBaseDns($criteria->getBaseDns());
		
		$new->addFilterNode($this->getFilters());
		$new->addFilterNode($criteria->getFilters());
		
		return $new;
	}
	
}
