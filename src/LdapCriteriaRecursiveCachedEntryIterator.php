<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;

/**
 * LdapCriteriaRecursiveCachedEntryIterator class file.
 * 
 * This class reassembles the LdapEntryIteratorInterface, LdapQueryFetchInterface
 * and LdapResultSetInterface without having to use the query fetch and the
 * result set structures.
 * 
 * This query result aggregates the full execution of all the result sets that
 * were fetched for each of the base dns of the criteria (and will be empty if
 * the criteria does not provides any base dn as no queries will be issued to
 * the ldap server), and stores the execution results of the recursive searches
 * for each of those queries, based on the primary key field of the models.
 * 
 * This query result supports the limit and offset of the given criteria, and
 * will adjust the given limit and criteria of the inner query results according
 * to the retrieved records for each query result and the recursive need of
 * each of those.
 * 
 * @author Anastaszor
 */
class LdapCriteriaRecursiveCachedEntryIterator extends LdapMultiCachedEntryIterator implements LdapEntryIteratorInterface
{
	
	/**
	 * Builds a new LdapCriteriaRecursiveCachedEntryIterator with the given
	 * factory, ldap_link, criteria and logger.
	 * 
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param \LDAP\Connection $ldapLink
	 * @param LdapCriteriaInterface $criteria
	 * @param LoggerInterface $logger
	 * @throws LdapThrowable
	 */
	public function __construct(LdapDistinguishedNameParserInterface $dnParser, \LDAP\Connection $ldapLink, LdapCriteriaInterface $criteria, LoggerInterface $logger)
	{
		// we clone the criteria to avoid side effects
		$currentOffset = $criteria->getOffset();
		$currentLimit = $criteria->getLimit();
		$entries = [];
		
		foreach($criteria->getBaseDns() as $baseDn)
		{
			$innerCrit = clone $criteria;
			$innerCrit->setOffset($currentOffset);
			$innerCrit->setLimit($currentLimit);
			$result = new LdapRecursiveCachedEntryIterator($dnParser, $ldapLink, $baseDn, $innerCrit, $logger, null);
			$entries[] = $result;
			
			$queryCount = $result->getQueryCount();
			
			if($currentOffset > $queryCount)
			{
				$currentOffset -= $queryCount;
				continue;
			}
			
			$queryCount -= $currentOffset;
			$currentOffset = 0;
			$currentLimit = \max(0, $currentLimit - $queryCount);
		}
		
		parent::__construct($entries);
	}
	
}
