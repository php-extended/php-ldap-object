<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapRecord class file.
 * 
 * This class is a simple implementation of the LdapRecordInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 */
abstract class LdapRecord implements LdapRecordInterface
{
	
	/**
	 * The ldap connection instance.
	 *
	 * @var ?LdapConnectionInterface
	 */
	public static ?LdapConnectionInterface $ldap = null;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getConnection()
	 */
	public static function getConnection() : LdapConnectionInterface
	{
		if(LdapRecord::$ldap instanceof LdapConnectionInterface)
		{
			/** @phpstan-ignore-next-line */
			return self::$ldap;
		}
		
		throw new LdapException('Impossible to find the connection object. Maybe you forgot to initialize a LdapConnection first ?');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getLdapObjectClass()
	 */
	public static function getLdapObjectClass() : string
	{
		return 'top';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getAttributes()
	 */
	public static function attributes() : array
	{
		return [
			'dn' => 'dn',
			'objectclass' => 'objectClass',
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::labels()
	 */
	public static function labels() : array
	{
		return [
			'dn' => 'Distinguished name',
			'objectClass' => 'Classes',
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getLabel()
	 */
	public static function getLabel(string $attribute) : string
	{
		$labels = static::labels();
		if(\array_key_exists($attribute, $labels))
		{
			return $labels[$attribute];
		}
		
		return $attribute;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::helpTexts()
	 */
	public static function helpTexts() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getHelpText()
	 */
	public static function getHelpText(string $attribute) : string
	{
		$htxs = static::helpTexts();
		if(\array_key_exists($attribute, $htxs))
		{
			return $htxs[$attribute];
		}
		
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getLdapBaseDistinguishedNames()
	 */
	public static function getLdapBaseDistinguishedNames() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getLdapClassCriteriaFilters()
	 */
	public static function getLdapClassCriteriaFilters() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getIdentifierField()
	 */
	public static function getIdentifierField() : string
	{
		return 'cn';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getUniqueFields()
	 */
	public static function getUniqueFields() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getAttributesClasses()
	 */
	public static function getAttributesClasses() : array
	{
		return ['objectClass' => 'string[]'];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::relations()
	 */
	public static function relations() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findByDn()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public static function findByDn(LdapDistinguishedNameInterface $dname, ?LdapCriteriaInterface $criteria = null) : ?LdapRecordInterface
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setRecordClass(static::class);
		$newcriteria->setBaseDn($dname);
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_BASE);
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return self::getConnection()->findObjects($newcriteria)->getFirstElement();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findByAttributes()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public static function findByAttributes(array $attributes = []) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		
		foreach($attributes as $attrname => $attrvalue)
		{
			$criteria->addValue($attrname, $attrvalue);
		}
		
		$criteria->setRecordClass(static::class);
		$criteria->setScope(LdapCriteria::LDAP_SCOPE_SUBTREE);
		$criteria->addAllBaseDns(static::getLdapBaseDistinguishedNames());
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return self::getConnection()->findObjects($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findAll()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public static function findAll(?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setRecordClass(static::class);
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_SUBTREE);
		$newcriteria->addAllBaseDns(static::getLdapBaseDistinguishedNames());
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return self::getConnection()->findObjects($newcriteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findAllByAttributes()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public static function findAllByAttributes(array $attributes = []) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		
		foreach($attributes as $attrname => $attrvalue)
		{
			$criteria->addValue($attrname, $attrvalue);
		}
		
		return self::findAll($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::count()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public static function count(?LdapCriteriaInterface $criteria = null) : int
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setRecordClass(static::class);
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_SUBTREE);
		$newcriteria->addAllBaseDns(static::getLdapBaseDistinguishedNames());
		
		return self::getConnection()->countObjects($newcriteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::countByAttributes()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public static function countByAttributes(array $attributes = []) : int
	{
		$criteria = new LdapCriteria();
		
		foreach($attributes as $attrname => $attrvalue)
		{
			$criteria->addValue($attrname, $attrvalue);
		}
		
		return self::count($criteria);
	}
	
	/**
	 * The unique distinguished name of the record. This is the primary key.
	 *
	 * @var LdapDistinguishedNameInterface
	 */
	protected LdapDistinguishedNameInterface $_dn;
	
	/**
	 * The object classes of the record. Ldap implements multi inheritance.
	 * As php does not support multi inheritance, objects are only instances
	 * of one class, which represents all the fields the multi inheritance
	 * at once.
	 * For backward compatibility, php Traits are not used in this version of
	 * the framework.
	 *
	 * @var array<integer, string>
	 */
	protected array $_objectClasses = [];
	
	/**
	 * The lower attribute names of this record (isset is faster than in_array).
	 *
	 * @var array<string, integer>
	 */
	protected array $_lowerAttributes = [];
	
	/**
	 * All the attributes values that are stored in this record. Most of the
	 * attributes are strings, or array of strings. All attributes values are
	 * stored as lower strings to avoid case sensitive behavior between data
	 * providers. (i.e. proxies).
	 *
	 * @var array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	protected array $_attributes = [];
	
	/**
	 * All the related objects that are already loaded for this record. Note
	 * that the relations which are face to face are not automatically mapped,
	 * i.e. if a record A have a relation to record B via keys a to b, the
	 * relation from record B to record A via keys b to a does not necessarily
	 * exists, and if it exists, the reciproc link between models will not be
	 * provided as the related models are loaded, to avoid circular references.
	 * If you make circular references to avoid searching through the ldap for
	 * relations, you will have to unset them later to avoid php circular
	 * reference memory leaks.
	 *
	 * @var array<string, null|LdapRecordInterface|array<integer, LdapRecordInterface>>
	 */
	protected array $_related = [];
	
	/**
	 * The parent record of this record. This is false by default, when this is
	 * not initialized, but is set to null if this object has no parent. 
	 * 
	 * @var ?LdapRecordInterface
	 */
	protected ?LdapRecordInterface $_parent = null;
	
	/**
	 * Builds a new LdapRecord based on the given dn and its object classes.
	 * 
	 * @param LdapDistinguishedNameInterface $ldn
	 * @param array<integer|string, string> $objectClasses
	 */
	public function __construct(LdapDistinguishedNameInterface $ldn, array $objectClasses)
	{
		$this->_dn = $ldn;
		$this->_objectClasses = \array_values($objectClasses);
	}
	
	/**
	 * This static method is called for classes exported by var_export()
	 * since PHP 5.1.0.
	 *
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $vars
	 * @return LdapRecord
	 * @psalm-suppress PossiblyUndefinedStringArrayOffset
	 */
	public static function __set_state(array $vars = []) : LdapRecord
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument,UnsafeInstantiation */
		$obj = new static($vars['_dn'], $vars['_objectClasses']);
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$obj->_lowerAttributes = $vars['_lowerAttributes'];
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$obj->_attributes = $vars['_attributes'];
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$obj->_related = $vars['_related'];
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$obj->_parent = $vars['_parent'];
		
		return $obj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::__get()
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function __get(string $name)
	{
		$sname = (string) \mb_strtolower($name);
		if('dn' === $sname)
		{
			return $this->getDn();
		}
		
		if('objectclass' === $sname)
		{
			return $this->_objectClasses;
		}
		
		if(isset($this->_attributes[$sname]))
		{
			return $this->_attributes[$sname];
		}
		
		// {{{ check for undefined attribute values
		if(isset(static::attributes()[$sname]))
		{
			if($this->isArrayType($sname))
			{
				return [];
			}
			
			return null;
		}
		// }}}
		
		if(isset($this->_related[$name]))
		{
			return $this->_related[$name];
		}
		
		$relations = $this->relations();
		if(!isset($relations[$name]))
		{
			$getter = 'get'.\ucfirst($name);
			if(\method_exists($this, $getter))
			{
				/** @psalm-suppress MixedReturnStatement */
				return $this->{$getter}();
			}
			
			$message = 'The field {name} is not defined in class {class}';
			$context = ['{name}' => $name, '{class}' => static::class];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		return $this->_related[$name] = $this->getRelated($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::__set()
	 */
	public function __set(string $name, $value) : void
	{
		$sname = (string) \mb_strtolower($name);
		// dn and objectclass cannot be modified
		if(\in_array($sname, ['dn', 'objectclass'], true))
		{
			return;
		}
		
		if(isset($this->attributes()[$sname]))
		{
			if(\array_key_exists($sname, \array_map('strtolower', $this->getAttributesClasses())))
			{
				if(\is_array($value))
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
					$this->_attributes[$sname] = $value;
					
					return;
				}
				
				$this->_attributes[$sname] = [$value];
				
				return;
			}
			
			/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
			$this->_attributes[$sname] = $value;
			
			return;
		}
		
		$setter = 'set'.\ucfirst($name);
		if(\method_exists($this, $setter))
		{
			$this->{$setter}($value);
			
			return;
		}
		if(\method_exists($this, 'get'.\ucfirst($name)))
		{
			$message = 'The field {name} is read-only in class {class}';
			$context = ['{name}' => $name, '{class}' => static::class];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		$message = 'The field {name} is not defined in class {class}';
		$context = ['{name}' => $name, '{class}' => static::class];
		
		throw new LdapException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::__isset()
	 */
	public function __isset(string $name) : bool
	{
		$sname = \mb_strtolower($name);
		
		if('dn' === $sname)
		{
			return !$this->_dn->isEmpty();
		}
		
		if('objectclass' === $sname)
		{
			return \count($this->_objectClasses) > 0;
		}
		
		if(isset($this->_attributes[$sname]))
		{
			return true;
		}
		
		$getter = 'get'.\ucfirst($name);
		if(\method_exists($this, $getter))
		{
			return $this->{$getter}() !== null;
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::__unset()
	 */
	public function __unset(string $name) : void
	{
		$sname = \mb_strtolower($name);
		
		// cant unset dn or objectclass
		if(\in_array($name, ['dn', 'objectclass'], true))
		{
			return;
		}
		
		if(isset($this->attributes()[$sname]))
		{
			unset($this->_attributes[$sname]);
			
			return;
		}
		
		$setter = 'set'.\ucfirst($name);
		if(\method_exists($this, $setter))
		{
			$this->{$setter}(null);
			
			return;
		}
		if(\method_exists($this, 'get'.\ucfirst($name)))
		{
			$message = 'The field {name} is read-only in class {class}';
			$context = ['{name}' => $name, '{class}' => static::class];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		$message = 'The field {name} is not defined in class {class}';
		$context = ['{name}' => $name, '{class}' => static::class];
		
		throw new LdapException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$chp = ['dn' => $this->_dn->__toString()];
		
		foreach($this->_attributes as $propname => $propval)
		{
			$propname = (string) $propname;
			if(null === $propval)
			{
				$chp[] = '"'.$propname.'":null';
				continue;
			}
			
			if(\is_array($propval))
			{
				$chp[] = '"'.$propname.'":array('.((string) \count($propval)).')';
				continue;
			}
			
			if(\is_object($propval))
			{
				$chp[] = '"'.$propname.'":object('.\get_class($propval).')';
				continue;
			}
			
			if(\is_string($propval))
			{
				$chp[] = '"'.$propname.'":"'.\str_replace(
					['\\', '"'],
					['\\\\', '\\"'],
					$propval,
				).'"';
				continue;
			}
			
			if(\is_bool($propval))
			{
				$chp[] = '"'.$propname.'":'.($propval ? 'true' : 'false');
				continue;
			}
			
			// is a number
			$chp[] = '"'.$propname.'":'.((string) $propval);
		}
		
		return '{'.static::class.'@'.\implode(', ', $chp).'}';
	}
	
	/**
	 * Gets whether the given attribute name represents an array type.
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function isArrayType(string $name) : bool
	{
		$sname = \mb_strtolower($name);
		$type = static::getAttributesClasses();
		
		return isset($type[$sname]) && (
			false !== \mb_strpos($type[$sname], '[]')
			|| false !== \mb_strpos($type[$sname], 'array(')
		);
	}
	
	/**
	 * Gets the relational data under object form.
	 * 
	 * @param string $name
	 * @return null|LdapRecordInterface|array<integer, LdapRecordInterface>
	 */
	public function getRelated(string $name)
	{
		$relations = $this->relations();
		if(!isset($relations[$name]))
		{
			return null;
		}
		
		/** @var LdapRelationInterface<LdapRecordInterface> $relation */
		$relation = $relations[$name];
		$query = $relation->getRelatedObjects($this);
		if(\in_array($relation->getTypeOfRelation(), [LdapRelationInterface::ONE_TO_ONE, LdapRelationInterface::MANY_TO_ONE], true))
		{
			return $query->getFirstElement();
		}
		
		// else $type === self::ONE_TO_MANY || $type === self::MANY_TO_MANY
		/** @var array<integer, LdapRecordInterface> $objects */
		$objects = [];
		
		/** @var LdapRecordInterface $object */
		foreach($query as $object)
		{
			$objects[] = $object;
		}
		
		return $objects;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getDn()
	 */
	public function getDn() : LdapDistinguishedNameInterface
	{
		return $this->_dn;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getObjectClasses()
	 */
	public function getObjectClasses() : array
	{
		return $this->_objectClasses;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::equals()
	 */
	public function equals($object) : bool
	{
		if(!$object instanceof LdapRecordInterface)
		{
			return false;
		}
		
		return \get_class($object) === static::class
			&& $this->getDn()->equals($object->getDn());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::contains()
	 */
	public function contains(LdapRecordInterface $record) : bool
	{
		return $this->getDn()->contains($record->getDn());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::isContainedBy()
	 */
	public function isContainedBy(LdapRecordInterface $record) : bool
	{
		return $record->getDn()->contains($this->getDn());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::isLeaf()
	 */
	public function isLeaf() : bool
	{
		return \in_array($this->getDn()->getFinalIdentifierField(), ['cn', 'uid'], true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getParent()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function getParent() : ?LdapRecordInterface
	{
		if(null === $this->_parent)
		{
			$criteria = new LdapCriteria();
			$criteria->addBaseDn($this->getDn()->getParentDistinguishedName());
			$criteria->setScope(LdapCriteria::LDAP_SCOPE_BASE);
			$result = $this->getConnection()->findObjects($criteria);
			$this->_parent = $result->getFirstElement();
		}
		
		return $this->_parent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 * @psalm-suppress InvalidReturnType
	 */
	public function getChildren(?string $childClass = null) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		if(null !== $childClass)
		{
			$criteria->setRecordClass($childClass);
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->findChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getChildrenCount()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function getChildrenCount(?string $childClass = null) : int
	{
		$criteria = new LdapCriteria();
		if(null !== $childClass)
		{
			$criteria->setRecordClass($childClass);
		}
		
		return $this->countChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getSubtreeChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 * @psalm-suppress InvalidReturnType
	 */
	public function getSubtreeChildren(?string $childClass = null) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		if(null !== $childClass)
		{
			$criteria->setRecordClass($childClass);
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->findSubtreeChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::getSubtreeChildrenCount()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function getSubtreeChildrenCount(?string $childClass = null) : int
	{
		$criteria = new LdapCriteria();
		if(null !== $childClass)
		{
			$criteria->setRecordClass($childClass);
		}
		
		return $this->countSubtreeChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function findChildren(?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setBaseDn($this->getDn());
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_ONE);
		
		return $this->getConnection()->findObjects($newcriteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findChildrenByAttributes()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function findChildrenByAttributes(array $attributes = []) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		
		foreach($attributes as $attrname => $attrvalue)
		{
			$criteria->addValue($attrname, $attrvalue);
		}
		
		return $this->findChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findSubtreeChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function findSubtreeChildren(?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setBaseDn($this->getDn());
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_SUBTREE);
		
		return $this->getConnection()->findObjects($newcriteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::findSubtreeChildrenByAttributes()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function findSubtreeChildrenByAttributes(array $attributes = []) : LdapObjectIteratorInterface
	{
		$criteria = new LdapCriteria();
		
		foreach($attributes as $attrname => $attrvalue)
		{
			$criteria->addValue($attrname, $attrvalue);
		}
		
		return $this->findChildren($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::countChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function countChildren(?LdapCriteriaInterface $criteria = null) : int
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setBaseDn($this->getDn());
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_ONE);
		
		return $this->getConnection()->countObjects($newcriteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapRecordInterface::countSubtreeChildren()
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function countSubtreeChildren(?LdapCriteriaInterface $criteria = null) : int
	{
		$newcriteria = new LdapCriteria();
		if(null !== $criteria)
		{
			$newcriteria = $newcriteria->mergeWith($criteria);
		}
		
		$newcriteria->setBaseDn($this->getDn());
		$newcriteria->setScope(LdapCriteria::LDAP_SCOPE_SUBTREE);
		
		return $this->getConnection()->countObjects($newcriteria);
	}
	
}
