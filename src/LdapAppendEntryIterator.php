<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use AppendIterator;
use Iterator;

/**
 * LdapAppendEntryIterator class file.
 * 
 * This class represents an entry iterator that appends other entry iterators.
 * 
 * @author Anastaszor
 * @extends \AppendIterator<integer, LdapEntryInterface, \Iterator<integer, LdapEntryInterface>>
 */
class LdapAppendEntryIterator extends AppendIterator implements LdapEntryIteratorInterface
{
	
	/**
	 * The cumulated query count.
	 *
	 * @var integer
	 */
	protected int $_queryCount = 0;
	
	/**
	 * Builds a new LdapMultiStreamEntryIterator with its inner iterators.
	 * 
	 * @param array<integer, LdapEntryIteratorInterface> $iterators
	 */
	public function __construct(array $iterators = [])
	{
		foreach($iterators as $iterator)
		{
			$this->append($iterator);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppendIterator::append()
	 * @param Iterator<LdapEntryInterface> $iterator
	 */
	public function append(Iterator $iterator) : void
	{
		if($iterator instanceof LdapEntryIteratorInterface)
		{
			$this->_queryCount += $iterator->getQueryCount();
			parent::append($iterator);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		/** @phpstan-ignore-next-line */
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getFirstElement()
	 */
	public function getFirstElement() : ?LdapEntryInterface
	{
		foreach($this as $entry)
		{
			return $entry;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::mergeWith()
	 */
	public function mergeWith(LdapEntryIteratorInterface $result) : LdapEntryIteratorInterface
	{
		return new self([$this, $result]);
	}
	
}
