<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;

/**
 * LdapMultiEntryIteratorCachedEntryIterator class file.
 * 
 * This class represents a local cache of the results that were directly fetched
 * from another multiple query results (that can cache them or give them on the
 * fly).
 * 
 * This query result does not handle criteria elements, and pulls all the
 * elements for all the given query results.
 * 
 * @author Anastaszor
 * @extends \ArrayIterator<integer, LdapEntryInterface>
 * @todo replace cached : [ multi entry iterator -> 1 entry iterator ] with stream
 */
class LdapMultiCachedEntryIterator extends ArrayIterator implements LdapEntryIteratorInterface
{
	
	/**
	 * The cumulated query count.
	 * 
	 * @var integer
	 */
	protected int $_queryCount = 0;
	
	/**
	 * Builds a new LdapMultiEntryIteratorCachedEntryIterator with the given previous
	 * query results.
	 * 
	 * @param array<integer, LdapEntryIteratorInterface> $results
	 */
	public function __construct(array $results)
	{
		$entries = [];
		
		foreach($results as $result)
		{
			/** @var LdapEntryInterface $record */
			foreach($result as $record)
			{
				$entries[] = $record;
			}
			$this->_queryCount += $result->getQueryCount();
		}
		
		parent::__construct($entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getFirstElement()
	 */
	public function getFirstElement() : ?LdapEntryInterface
	{
		foreach($this as $entry)
		{
			return $entry;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::getQueryCount()
	 */
	public function getQueryCount() : int
	{
		return $this->_queryCount;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapEntryIteratorInterface::mergeWith()
	 */
	public function mergeWith(LdapEntryIteratorInterface $result) : LdapEntryIteratorInterface
	{
		return new self([$this, $result]);
	}
	
}
