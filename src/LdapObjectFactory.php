<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * LdapObjectFactory class file.
 * 
 * This class is a simple implementation of the LdapObjectFactoryInterface.
 * 
 * @author Anastaszor
 */
abstract class LdapObjectFactory implements LdapObjectFactoryInterface
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new LdapObjectFactory with the given logger.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(?LoggerInterface $logger = null)
	{
		$this->_logger = $logger ?? new NullLogger();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Try to find the object class of the object this factory should build,
	 * according to its objectClass array. The returned class-string should
	 * extend LdapModel.
	 * 
	 * Warning if the classes you made are not equal to 1 model class <=> 1
	 * objectClass element. This factory should get the right class name
	 * according to the specifications you made.
	 * 
	 * This method MUST be implemented by inherited factories.
	 * 
	 * @param array<integer|string, string> $objectClasses
	 * @return ?class-string<LdapRecord> the class of the model, null if not found
	 * @throws LdapThrowable if something goes wrong
	 */
	abstract public function guessObjectClass(array $objectClasses = []) : ?string;
	
	/**
	 * Builds the record according to the information given by the entry.
	 * If the class of target object can't be guessed, an exception will 
	 * be thrown.
	 * 
	 * @param LdapEntryInterface $entry
	 * @return LdapRecordInterface
	 * @throws LdapThrowable if the object class cannot be resolved
	 */
	public function buildObjectEntry(LdapEntryInterface $entry) : LdapRecordInterface
	{
		$objectClasses = $entry->getObjectClasses();
		$class = $this->guessObjectClass($objectClasses);
		if(null === $class)
		{
			$message = 'Failed to find the object class for object with classes {classes}';
			$context = ['{classes}' => '['.\implode(', ', $objectClasses).']'];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		/** @phpstan-ignore-next-line */
		if(!\is_subclass_of($class, LdapRecord::class, true))
		{
			$message = 'The found object class {class} for object with objectClasses [{object}] does not extend the required model class {model}';
			$context = [
				'{class}' => $class,
				'{object}' => \implode(', ', $objectClasses),
				'{model}' => LdapRecord::class,
			];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		return $this->buildObject($class, $entry->getDn(), $objectClasses, $entry->getAttributes());
	}
	
	/**
	 * Builds an $classname object with the given attribute values.
	 * 
	 * @param class-string $classname
	 * @param LdapDistinguishedNameInterface $ldn
	 * @param array<integer, string> $objectClasses
	 * @param array<string, int|float|string|array<integer, int|float|string>> $attributes
	 * @return LdapRecordInterface
	 * @throws LdapThrowable
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function buildObject(string $classname, LdapDistinguishedNameInterface $ldn, array $objectClasses, array $attributes = []) : LdapRecordInterface
	{
		/** @var LdapRecord $object */ /** @psalm-suppress MixedMethodCall */
		$object = new $classname($ldn, $objectClasses);
		
		$lowerfieldnames = [];
		
		/** @psalm-suppress MixedMethodCall */
		foreach($object->attributes() as $fieldname)
		{
			$lowerfieldnames[(string) \mb_strtolower((string) $fieldname)] = (string) $fieldname;
		}
		
		/** @psalm-suppress MixedMethodCall */
		$fieldtypes = $object->getAttributesClasses();
		
		foreach($attributes as $key => $vars)
		{
			if(!\is_array($vars))
			{
				$vars = [$vars];
			}
			
			$pkey = (string) \preg_replace('#[^\\d\\w_]#', '_', $key);
			$lkey = \mb_strtolower($pkey);
			
			// if the attribute is a specific field name, then we should
			// see in the object if we can set it
			if(!isset($lowerfieldnames[$lkey]))
			{
				// we couldnt set that attribute, may be a model error ?
				$message = 'Failed to find attribute {attrname} in object of class {class} for dn {dn} and objectclasses [{objectclass}]';
				$context = [
					'{attrname}' => $key,
					'{class}' => $classname,
					'{dn}' => $ldn->getStringRepresentation(),
					'{objectclass}' => \implode(',', $objectClasses),
				];
				$this->_logger->error($message, $context);
				continue;
			}
			
			$fieldname = $lowerfieldnames[$lkey];
			// we can set it. We should see in whattype we will set it.
			$type = 'string';
			if(isset($fieldtypes[$fieldname]))
			{
				/** @psalm-suppress MixedArrayAccess */
				$type = $fieldtypes[$fieldname];
			}
			
			try
			{
				/** @psalm-suppress ArgumentTypeCoercion,MixedArgument */
				$this->setObjectAttributeByType($object, $fieldname, $type, $vars);
			}
			catch(LdapException $exc)
			{
				$cpt = 0;

				do
				{
					$message = 'Failed to set object attribute of class {class} for field {field} and type {type}, {msg}';
					$context = [
						'class' => \get_class($object),
						'field' => $fieldname,
						'type' => $type,
						'{msg}' => $exc->getMessage(),
					];
					$this->_logger->error($message, $context);
					$exc = $exc->getPrevious();
					$cpt++;
				}
				while(null !== $exc && 10 > $cpt);
			}
		}
		
		/** @psalm-suppress LessSpecificReturnStatement */
		return $object;
	}
	
	/**
	 * Sets an attribute in given object according to the type it should be.
	 * If the attribute is not the given type, it will be converted to fit
	 * the type it should be. See *ify function for type conversions. For the
	 * array part, if the field should be an array but is not, it will be 
	 * converted to an array with 1 element, if the field should not be an
	 * array but is, only its first element will be taken into account.
	 * 
	 * @param LdapRecordInterface $object
	 * @param string $fieldname
	 * @param string $type
	 * @param array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>> $value the value from ldap api
	 * @return LdapRecordInterface
	 * @throws LdapThrowable if the given class does not exist
	 */
	public function setObjectAttributeByType(LdapRecordInterface $object, string $fieldname, string $type, array $value) : LdapRecordInterface
	{
		if(!isset($value[0]))
		{
			return $object;
		}
		
		try
		{
			$value = $this->getCoercedAttributeForType($type, $value);
		}
		catch(LdapException $e)
		{
			$message = 'Failed to coerce attribute "{name}" for type "{type}" on object {class}';
			$context = ['{name}' => $fieldname, '{type}' => $type, '{class}' => \get_class($object)];
			
			throw new LdapException(\strtr($message, $context), -1, $e);
		}
		
		if(null !== $value)
		{
			$object->__set($fieldname, $value);
		}
		
		return $object;
	}
	
	/**
	 * Gets the attribute coerced to its real type.
	 * 
	 * @param string $type
	 * @param array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return null|bool|int|float|string|object|array<integer, bool|int|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws LdapThrowable if the given class does not exist
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getCoercedAttributeForType(string $type, array $value)
	{
		if(false !== \mb_strpos($type, 'array(') || false !== \mb_strpos($type, '[]'))
		{
			return $this->getCoercedAttributeArray($type, $value);
		}
		
		$value = \array_values($value);
		
		if(!isset($value[0]))
		{
			$message = 'The value should not be an empty array for type "{type}"';
			$context = ['{type}' => $type];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		if(\count($value) > 1)
		{
			$message = 'The type "{type}" cannot be used with value "{value}", because its has more than 1 value ({k})';
			$context = ['{type}' => $type, '{value}' => \json_encode($value), '{k}' => \count($value)];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		if(\in_array($type, ['int', 'integer'], true))
		{
			/** @psalm-suppress RiskyCast */
			return (int) $value[0];
		}
		
		if(\in_array($type, ['bool', 'boolean'], true))
		{
			return (bool) $value[0];
		}
		
		if(\in_array($type, ['float', 'double', 'real'], true))
		{
			/** @psalm-suppress RiskyCast */
			return (float) $value[0];
		}
		
		if(\in_array($type, ['string', ''], true))
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
			return (string) $value[0];
		}
		
		// expect this as a custom type
		if(!\class_exists($type))
		{
			$message = 'The class {class} does not exists or id not loadable.';
			$context = ['{class}' => $type];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		/** @psalm-suppress MixedMethodCall */
		return new $type($value[0]);
	}
	
	/**
	 * Gets the coerced attribute value in array form.
	 * 
	 * @param string $type
	 * @param array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer, bool|int|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws LdapThrowable
	 */
	public function getCoercedAttributeArray(string $type, array $value) : array
	{
		$objecttype = \str_replace(['array(', ')', '[', ']'], '', $type);
		unset($value['count']);
		$values = [];
		
		foreach($value as $str)
		{
			$newval = $this->getCoercedAttributeForType($objecttype, [$str]);
			
			if(null === $newval)
			{
				continue;
			}
			
			if(\is_array($newval))
			{
				$values += $newval;
				continue;
			}
			
			$values[] = $newval;
		}
		
		return $values;
	}
	
}
