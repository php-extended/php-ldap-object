<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * LdapDirectory interface file.
 * 
 * This class holds the basic components for it to be an ldap directory.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class LdapDirectory implements LdapDirectoryInterface
{
	
	/**
	 * The logger for this class.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The configuration of this annuaire.
	 *
	 * @var LdapConfigurationInterface
	 */
	protected LdapConfigurationInterface $_configuration;
	
	/**
	 * The distinguished name parser for objects.
	 * 
	 * @var LdapDistinguishedNameParserInterface
	 */
	protected LdapDistinguishedNameParserInterface $_dnParser;
	
	/**
	 * The object factory to build objects.
	 * 
	 * @var LdapObjectFactoryInterface
	 */
	protected LdapObjectFactoryInterface $_factory;
	
	/**
	 * The session data about the user.
	 *
	 * @var ?LdapUserSessionInterface
	 */
	protected ?LdapUserSessionInterface $_session = null;
	
	/**
	 * The current hierarchy available.
	 *
	 * @var ?LdapHierarchy
	 */
	protected ?LdapHierarchy $_hierarchy = null;
	
	/**
	 * Builds a new LdapDirectory with the given configuration and factory.
	 * 
	 * @param LdapConfigurationInterface $configuration
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param LdapObjectFactoryInterface $factory
	 * @param ?LoggerInterface $logger
	 */
	public function __construct(LdapConfigurationInterface $configuration, LdapDistinguishedNameParserInterface $dnParser, LdapObjectFactoryInterface $factory, ?LoggerInterface $logger = null)
	{
		$this->_configuration = $configuration;
		$this->_dnParser = $dnParser;
		$this->_factory = $factory;
		$this->_logger = $logger ?? new NullLogger();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::getBaseDn()
	 */
	public function getBaseDn() : LdapDistinguishedNameInterface
	{
		return $this->_configuration->getSuffixDn();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::checkLogin()
	 */
	public function checkLogin(LdapUserSessionInterface $user) : bool
	{
		$dname = $this->guessDnFromUsername($user->getIdentifier());
		
		try
		{
			$oldConnection = LdapRecord::$ldap;
			$newConnection = new LdapConnection($this->_configuration, $this->_dnParser, $this->_factory, $this->_logger);
			$newConnection->login($dname->getStringRepresentation(), $user->getPassword());
			
			LdapRecord::$ldap = $oldConnection ?? $newConnection;
			if(null === $oldConnection)
			{
				LdapRelation::$dnParser = $this->_dnParser;
			}
		}
		catch(LdapException $e)
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::checkAccess()
	 */
	public function checkAccess(LdapUserSessionInterface $user, ?string $application = null, ?string $role = null) : bool
	{
		return $this->checkLogin($user);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::connect()
	 */
	public function connect(LdapUserSessionInterface $user) : LdapConnectionInterface
	{
		$dname = $this->guessDnFromUsername($user->getIdentifier());
		
		try
		{
			$newConnection = new LdapConnection($this->_configuration, $this->_dnParser, $this->_factory, $this->_logger);
			$newConnection->login($dname->getStringRepresentation(), $user->getPassword());
			LdapRecord::$ldap = $newConnection;
			LdapRelation::$dnParser = $this->_dnParser;
		}
		catch(LdapException $e)
		{
			$message = 'Failed to connect ldap directory {this} with user {user}.';
			$context = ['{this}' => static::class, '{user}' => $user->__toString()];
			
			throw new LdapException(\strtr($message, $context), (int) $e->getCode(), $e);
		}
		
		return $newConnection;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::guessDnFromUsername()
	 */
	public function guessDnFromUsername(?string $username) : LdapDistinguishedNameInterface
	{
		$dname = clone $this->getBaseDn();
		
		if(null !== $username)
		{
			$dname = $dname->append('uid', $username);
		}
		
		return $dname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDirectoryInterface::createHierarchy()
	 */
	public function createHierarchy(LdapRecordInterface $record) : LdapHierarchyInterface
	{
		if(null === $this->_hierarchy || !$this->_hierarchy->getTarget()->getDn()->equals($record->getDn()))
		{
			$this->_hierarchy = new LdapHierarchy($record, $this->getBaseDn());
		}
		
		return $this->_hierarchy;
	}
	
}
