<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapNativeUserSession class file.
 * 
 * This class represents an user session that is stored and retrieved from the
 * native php session mechanism.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.Superglobals")
 */
class LdapNativeUserSession implements LdapUserSessionInterface
{
	
	/**
	 * The key that is used in the session to retrieve the user identifier.
	 * 
	 * @var string
	 */
	public static string $sessionIdentifierKey = 'PhpExtended/Ldap/LdapNativeUserSession::__identifier__';
	
	/**
	 * The key that is used in the session to retieve the user password.
	 * 
	 * @var string
	 */
	public static string $sessionPasswordKey = 'PhpExtended/Ldap/LdapNativeUserSession::__password__';
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::create()
	 */
	public static function create(?string $username = null, ?string $password = null) : LdapUserSessionInterface
	{
		return new self($username, $password);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::load()
	 */
	public static function load() : ?LdapUserSessionInterface
	{
		$status = \session_status();
		if(\PHP_SESSION_DISABLED === $status)
		{
			return null;
		}
		
		if(\PHP_SESSION_NONE === $status)
		{
			if(!\session_start())
			{
				return null;
			}
		}
		
		$username = $password = null;
		
		/** @psalm-suppress PossiblyInvalidArrayOffset */
		if(isset($_SESSION[self::$sessionIdentifierKey]) && \is_scalar($_SESSION[self::$sessionIdentifierKey]))
		{
			/** @psalm-suppress PossiblyInvalidArrayOffset */
			$username = (string) $_SESSION[self::$sessionIdentifierKey];
		}
		
		/** @psalm-suppress PossiblyInvalidArrayOffset */
		if(isset($_SESSION[self::$sessionPasswordKey]) && \is_scalar($_SESSION[self::$sessionPasswordKey]))
		{
			/** @psalm-suppress PossiblyInvalidArrayOffset */
			$password = (string) $_SESSION[self::$sessionPasswordKey];
		}
		
		return new self($username ?? null, null === $username ? ($password ?? null) : null);
	}
	
	/**
	 * The encrypted identifier of the session user.
	 *
	 * @var ?string
	 */
	protected ?string $_identifier;
	
	/**
	 * The encrypted password of the session user.
	 *
	 * @var ?string
	 */
	protected ?string $_password;
	
	/**
	 * Builds a new LdapNativeUserSession with its data.
	 * 
	 * @param ?string $identifier
	 * @param ?string $password
	 */
	public function __construct(?string $identifier = null, ?string $password = null)
	{
		$this->_identifier = $identifier;
		$this->_password = $password;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return \strlen((string) $this->_identifier) > 0 ? (string) $this->_identifier : '(anonymous)';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::isAnonymous()
	 */
	public function isAnonymous() : bool
	{
		return null === $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::getIdentifier()
	 */
	public function getIdentifier() : ?string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::getPassword()
	 */
	public function getPassword() : ?string
	{
		return $this->_password;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::save()
	 */
	public function save() : bool
	{
		$status = \session_status();
		if(\PHP_SESSION_DISABLED === $status)
		{
			return false;
		}
		
		if(\PHP_SESSION_NONE === $status)
		{
			if(!\session_start())
			{
				return false;
			}
		}
		
		$_SESSION[self::$sessionIdentifierKey] = $this->_identifier;
		if(null === $this->_identifier)
		{
			unset($_SESSION[self::$sessionIdentifierKey]);
		}
		
		$_SESSION[self::$sessionPasswordKey] = $this->_password;
		if(null === $this->_password)
		{
			unset($_SESSION[self::$sessionPasswordKey]);
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapUserSessionInterface::logout()
	 */
	public function logout() : bool
	{
		$this->_identifier = null;
		$this->_password = null;
		
		return $this->save();
	}
	
	/**
	 * Destructor. Saves this object and writes the session. Warning not to
	 * destroy this object if others session related treatments should have
	 * been done, as this closes the session.
	 */
	public function __destruct()
	{
		\PHP_SESSION_ACTIVE === \session_status() && \session_write_close();
	}
	
}
