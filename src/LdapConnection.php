<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * LdapConnection class file.
 * 
 * This class is a simple implementation of the LdapConnectionInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class LdapConnection implements LdapConnectionInterface
{
	
	/**
	 * The configuration settings, which should be given to the constructor.
	 * 
	 * @var LdapConfigurationInterface
	 */
	protected LdapConfigurationInterface $_config;
	
	/**
	 * The factory to build the objects from the raw data.
	 *
	 * @var LdapObjectFactoryInterface
	 */
	protected LdapObjectFactoryInterface $_factory;
	
	/**
	 * The dn parser.
	 * 
	 * @var LdapDistinguishedNameParserInterface
	 */
	protected LdapDistinguishedNameParserInterface $_dnParser;
	
	/**
	 * The logger to log events.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Null if the connection has not tried to be connected.
	 * True if the connection is in state "connected".
	 * False if the connection is in state "failed".
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_opened = null;
	
	/**
	 * Null if the connection has not tried to be binded.
	 * True if the connection is binded successfully.
	 * False if the connection has failed to bind.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_binded = null;
	
	/**
	 * Null if the connection has not tried to be binded.
	 * Empty string if the connection has tried to be binded anonymously.
	 * Non empty string if the connection has tried to be binded with authentication.
	 * 
	 * @var ?string
	 */
	protected ?string $_bindedWith = null;
	
	/**
	 * Null if the connection is in state "uninitialized" or failed to connect.
	 * 
	 * @var ?\LDAP\Connection
	 */
	protected $_link = null;
	
	/**
	 * Constructor of the connection. This does not open the connection, this
	 * only prepares it.
	 * 
	 * @param LdapConfigurationInterface $config
	 * @param LdapDistinguishedNameParserInterface $dnParser
	 * @param LdapObjectFactoryInterface $factory
	 * @param LoggerInterface $logger
	 * @throws LdapThrowable if there is no LDAP php extension
	 */
	public function __construct(LdapConfigurationInterface $config, LdapDistinguishedNameParserInterface $dnParser, LdapObjectFactoryInterface $factory, ?LoggerInterface $logger = null)
	{
		if(!\extension_loaded('ldap'))
		{
			throw new LdapException('The ldap extension should be activated.');
		}
		
		$this->_factory = $factory;
		$this->_dnParser = $dnParser;
		$this->_config = $config;
		$this->_logger = $logger ?? new NullLogger();
		
		// initializes the ldap connection to this instance of all the records
		if(null === LdapRecord::$ldap)
		{
			LdapRecord::$ldap = $this;
		}
		
		if(null === LdapRelation::$dnParser)
		{
			LdapRelation::$dnParser = $dnParser;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.((string) $this->_bindedWith).']::'.$this->_config->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::isOpen()
	 */
	public function isOpen() : bool
	{
		return true === $this->_opened;
	}
	
	/**
	 * Gets whether the given open operation has been tried.
	 * 
	 * @return boolean
	 */
	public function hasOpenBeenTried() : bool
	{
		return null !== $this->_opened;
	}
	
	/**
	 * Tries to open the connection to the server. This will pass the state of
	 * the connection from "not tried" to "connected" or "connection failed"
	 * according to ldap connection syntax checks.
	 * 
	 * Like for the underlying ldap library, this method does not contact the
	 * server for ldap binding, but checks that the ldap server is available
	 * by trying to "ping" it (by opening a socket).
	 * 
	 * @return boolean
	 */
	public function open() : bool
	{
		if($this->isOpen())
		{
			return true;
		}
		
		if($this->hasOpenBeenTried())
		{
			return false;
		}
		
		$this->_opened = true;
		$fullhost = $this->_config->isSecure() ? 'ldaps://' : 'ldap://';
		$fullhost .= $this->_config->getHostname().':'.((string) $this->_config->getPort());
		
		if(!$this->serviceping($this->_config->getHostname(), $this->_config->getPort()))
		{
			$this->_opened = false;
			$message = 'Failed to ping server {host}';
			$context = ['{host}' => $fullhost];
			
			throw new LdapException(\strtr($message, $context));
		}
		
		\set_error_handler(function()
		{ return false; });
		\error_clear_last();	// just in case
		$oldErrLvl = \error_reporting(\E_ALL ^ \E_WARNING);
		
		$res = \ldap_connect($fullhost);
		
		\error_reporting($oldErrLvl); // put it back
		$lastErr = \error_get_last();
		\restore_error_handler();
		
		$context = [
			'config' => $this->_config->__toString(),
			'errno' => $lastErr['type'] ?? 0,
			'errmsg' => $lastErr['message'] ?? 'success',
		];
		
		if(false === $res)
		{
			$this->_opened = false;
			$message = 'Failed to connect to {config} : {errno} {errmsg}';
			
			throw new LdapException(\strtr($message, $context));
		}
		
		$this->_link = $res;
		$this->_logger->info('Connection to {config} : {errno} {errmsg}', $context);
		
		foreach([
			\LDAP_OPT_PROTOCOL_VERSION => $this->_config->getVersion(),
			\LDAP_OPT_DEREF => $this->_config->getDereferencePolicy(),
			\LDAP_OPT_SIZELIMIT => $this->_config->getSizeLimit(),
			\LDAP_OPT_TIMELIMIT => $this->_config->getTimeLimit(),
			\LDAP_OPT_REFERRALS => $this->_config->followsReferral(),
			\LDAP_OPT_NETWORK_TIMEOUT => $this->_config->getConnectionTimeout(),
		] as $optCst => $optValue)
		{
			\ldap_set_option($this->_link, $optCst, $optValue);
		}
		
		return $this->_opened = true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::isBinded()
	 */
	public function isBinded() : bool
	{
		return true === $this->_binded;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::isLoggedWith()
	 */
	public function isLoggedWith() : ?string
	{
		return $this->_bindedWith;
	}
	
	/**
	 * Gets whether the given open binding operation has been tried for the
	 * given name.
	 * 
	 * @param ?string $name the binded login
	 * @return boolean
	 */
	public function hasBindedBeenTried(?string $name = null) : bool
	{
		return null !== $this->_binded && (string) $name === $this->_bindedWith;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::login()
	 */
	public function login(?string $login = null, ?string $password = null) : bool
	{
		if(!$this->open())
		{
			return false;
		}
		
		if($this->hasBindedBeenTried($login))
		{
			return $this->isBinded();
		}
		
		$this->_bindedWith = (string) $login;
		
		if('' === (\trim((string) $login)))
		{
			$login = null;
			$password = null;
		}
		if(null === $this->_link)
		{
			return false;
		}
		
		\set_error_handler(function()
		{ return false; });
		\error_clear_last();	// just in case
		$oldErrLvl = \error_reporting(\E_ALL ^ \E_WARNING);
		
		$this->_binded = (null === $login)
			? $this->_binded = \ldap_bind($this->_link)
			: $this->_binded = \ldap_bind($this->_link, $login, (string) $password);
		
		\error_reporting($oldErrLvl); // put it back
		$errno = \ldap_errno($this->_link);
		\restore_error_handler();
		
		$context = [
			'login' => (null === $login ? 'Anonymous User' : $login),
			'errno' => $errno,
			'errmsg' => \ldap_err2str($errno),
		];
		
		if(!$this->_binded)
		{
			$message = 'Failed to bind with {login} : {errno} {errmsg}';
			
			throw new LdapException(\strtr($message, $context), (int) \ldap_error($this->_link));
		}
		
		$this->_logger->info('Bind with {login} : {errno} {errmsg}', $context);
		
		return $this->_binded;
	}
	
	/**
	 * Counts the objects that matches the given criteria into the ldap.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return integer
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function countObjects(LdapCriteriaInterface $criteria) : int
	{
		return $this->countEntries($criteria);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::countEntries()
	 */
	public function countEntries(LdapCriteriaInterface $criteria) : int
	{
		if($criteria->isEmpty())
		{
			return 0;
		}
		
		if(!$this->isBinded())
		{
			$this->login($this->_config->getLogin(), $this->_config->getPassword());
		}
		
		if(null === $this->_link)
		{
			return 0;
		}
		
		$criteria->setLimit(0);
		$criteria->setOffset(\PHP_INT_MAX);
		if(empty($criteria->getBaseDns()))
		{
			$criteria->addBaseDn($this->_config->getSuffixDn());
		}
		
		$result = new LdapCriteriaRecursiveCachedEntryIterator($this->_dnParser, $this->_link, $criteria, $this->_logger);
		
		return $result->getQueryCount();
	}
	
	/**
	 * Searches the ldap for the requested objects that matches given criteria.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 * @throws LdapThrowable if the connection fails to open
	 * @throws LdapThrowable if the objects fails to be built
	 */
	public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
	{
		return new LdapEntryIteratorObjectIterator($this->_factory, [$this->findEntries($criteria)]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapConnectionInterface::findEntries()
	 */
	public function findEntries(LdapCriteriaInterface $criteria) : LdapEntryIteratorInterface
	{
		if($criteria->isEmpty())
		{
			return new LdapArrayEntryIterator();
		}
		
		if(!$this->isBinded())
		{
			$this->login($this->_config->getLogin(), $this->_config->getPassword());
		}
		
		if(null === $this->_link)
		{
			return new LdapArrayEntryIterator();
		}
		
		if(empty($criteria->getBaseDns()))
		{
			$criteria->addBaseDn($this->_config->getSuffixDn());
		}
		
		return new LdapCriteriaRecursiveCachedEntryIterator($this->_dnParser, $this->_link, $criteria, $this->_logger);
	}
	
	/**
	 * Closes the connection and destroys the link.
	 */
	public function close() : void
	{
		if($this->isOpen())
		{
			$this->_opened = false;
			$message = 'Closing connection to {config}';
			$context = ['config' => $this->_config->__toString()];
			$this->_logger->info($message, $context);
			if(null !== $this->_link)
			{
				$res = \ldap_unbind($this->_link);
				if(!$res)
				{
					$message = 'Error when closing connection to {config}';
					$context = ['config' => $this->_config->__toString()];
					$this->_logger->warning($message, $context);
				}
				$this->_link = null;
			}
		}
	}
	
	/**
	 * Executes a ping to given server. This is made to control the timeout
	 * as ldap_connect cannot guarantee to connect and cannot be controlled
	 * by timeout, as if the server is absent.
	 * 
	 * It seems like 1 second is not enough, we dont want to overload the server,
	 * so 5s  is set as default timeout.
	 * 
	 * @param string $host the server host to ping
	 * @param integer $port the server port to ping on
	 * @param integer $timeout the number of seconds needed for the ping
	 * @return boolean wether the connection fails
	 */
	protected function serviceping(string $host, int $port = 389, int $timeout = 5) : bool
	{
		if(\extension_loaded('sockets') && $this->_config->isServicePingEnabled())
		{
			$errno = $errstr = null;
			
			// https://stackoverflow.com/questions/272361/how-can-i-handle-the-warning-of-file-get-contents-function-in-php#272377
			// an error handler with return false should be used to have error_get_last() to work correctly
			// and we need to restore the previous error handler after getting contents
			/* {{{ */ \set_error_handler(function()
			{ return false; });
			/* ||| */ \error_clear_last();	// just in case
			$level = \error_reporting(0); // disable error reporting
			$sock = \fsockopen($host, $port, $errno, $errstr, $timeout);
			\error_reporting($level); // enable error reporting back
			/* ||| */ $error = \error_get_last();
			/* }}} */ \restore_error_handler();
			
			if(false === $sock || null !== $error)
			{
				return false; // DC is N/A
			}
			
			\fclose($sock); // explicitly close open socket connection
			
			return true; // DC is up & running, we can safely connect with ldap_connect
		}

		// suppose by default that if the service ping cannot be done,
		// everything is well unless proved otherwise
		return true;
	}
	
	/**
	 * Destructor. This guarantees the correct destruction of the connection.
	 */
	public function __destruct()
	{
		$this->close();
	}
	
}
