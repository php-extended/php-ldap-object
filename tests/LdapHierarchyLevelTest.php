<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapArrayObjectIterator;
use PhpExtended\Ldap\LdapConnection;
use PhpExtended\Ldap\LdapCriteriaInterface;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapHierarchyLevel;
use PhpExtended\Ldap\LdapHierarchyLevelInterface;
use PhpExtended\Ldap\LdapObjectIteratorInterface;
use PhpExtended\Ldap\LdapRecord;
use PHPUnit\Framework\TestCase;

class LdapHierarchyTestLdapRecord extends LdapRecord
{
	// nothing to add
}

class LdapHierarchyTestLdapConnection extends LdapConnection
{
	public function __construct() {}
}

/**
 * LdapHierarchyLevelTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapHierarchyLevel
 *
 * @internal
 *
 * @small
 */
class LdapHierarchyLevelTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapHierarchyLevelInterface
	 */
	protected LdapHierarchyLevelInterface $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Ldap\\LdapHierarchyLevel@level[2/3]', $this->_object->__toString());
	}
	
	public function testGetTarget() : void
	{
		$expected = new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'foobar']), ['objectClass']);
		$this->assertEquals($expected, $this->_object->getTarget());
	}
	
	public function testIsFirst() : void
	{
		$this->assertFalse($this->_object->isFirst());
	}
	
	public function testIsLast() : void
	{
		$this->assertFalse($this->_object->isLast());
	}
	
	public function testGetLevel() : void
	{
		$this->assertEquals(2, $this->_object->getLevel());
	}
	
	public function testGetNeighbors() : void
	{
		LdapRecord::$ldap = new class() extends LdapHierarchyTestLdapConnection
		{
			
			public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
			{
				return new LdapArrayObjectIterator();
			}
			
		};
		
		$k = 0;
		
		foreach($this->_object->getNeighbors() as $object)
		{
			$k++;
			unset($object);
		}
		$this->assertEquals(0, $k);
	}
	
	public function testGetNeighbors2() : void
	{
		LdapRecord::$ldap = new class() extends LdapHierarchyTestLdapConnection
		{
			
			public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
			{
				return new LdapArrayObjectIterator([
					new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
				]);
			}
			
		};
		
		$k = 0;
		
		foreach($this->_object->getNeighbors() as $object)
		{
			$k++;
			unset($object);
		}
		$this->assertEquals(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapHierarchyLevel(new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'foobar']), ['objectClass']));
		$this->_object->setPositionAt(2, 3);
	}
	
}
