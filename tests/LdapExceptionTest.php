<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapException;
use PHPUnit\Framework\TestCase;

/**
 * LdapExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapException
 *
 * @internal
 *
 * @small
 */
class LdapExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapException
	 */
	protected LdapException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapException();
	}
	
}
