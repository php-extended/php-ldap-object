<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapNativeUserSession;
use PHPUnit\Framework\TestCase;

/**
 * LdapNativeUserSessionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapNativeUserSession
 *
 * @internal
 *
 * @small
 */
class LdapNativeUserSessionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapNativeUserSession
	 */
	protected LdapNativeUserSession $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('identifier', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapNativeUserSession('identifier', 'password');
	}
	
}
