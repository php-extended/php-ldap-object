<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeMultiInterface;
use PhpExtended\Ldap\LdapQuery;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * LdapQueryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapQuery
 *
 * @internal
 *
 * @small
 */
class LdapQueryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapQuery
	 */
	protected LdapQuery $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('READ AT  FILTER  OFFSET 0 LIMIT 1000', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapQuery(
			\ldap_connect('ldap://hostname.example:389'),
			new LdapDistinguishedNameParser(),
			new NullLogger(),
			new LdapDistinguishedName(),
			new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND),
		);
	}
	
}
