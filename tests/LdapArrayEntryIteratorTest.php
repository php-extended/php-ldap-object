<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapArrayEntryIterator;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapEntry;
use PHPUnit\Framework\TestCase;

class LdapArrayEntryIteratorTestLdapEntry extends LdapEntry
{
	// nothing to add
}

/**
 * LdapArrayEntryIteratorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapArrayEntryIterator
 *
 * @internal
 *
 * @small
 */
class LdapArrayEntryIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapArrayEntryIterator
	 */
	protected LdapArrayEntryIterator $_object;
	
	public function testToString() : void
	{
		$class = \get_class($this->_object);
		$this->assertEquals($class.'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testGetFirstElement() : void
	{
		$this->assertInstanceOf(LdapEntry::class, $this->_object->getFirstElement());
	}
	
	public function testGetNoneFirstElement() : void
	{
		$this->assertNull((new LdapArrayEntryIterator([]))->getFirstElement());
	}
	
	public function testGetQueryCount() : void
	{
		$this->assertEquals(1, $this->_object->getQueryCount());
	}
	
	public function testMergeWith() : void
	{
		$newqr = new LdapArrayEntryIterator([
			new LdapArrayEntryIteratorTestLdapEntry(new LdapDistinguishedName(), []),
		]);
		
		$merged = $this->_object->mergeWith($newqr);
		
		$this->assertEquals(2, $merged->getQueryCount());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapArrayEntryIterator([
			new LdapArrayEntryIteratorTestLdapEntry(new LdapDistinguishedName(), []),
		]);
	}
	
}
