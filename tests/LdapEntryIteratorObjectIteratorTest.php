<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapEntryIteratorObjectIterator;
use PhpExtended\Ldap\LdapObjectFactoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapEntryIteratorObjectIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapEntryIteratorObjectIterator
 *
 * @internal
 *
 * @small
 */
class LdapEntryIteratorObjectIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapEntryIteratorObjectIterator
	 */
	protected LdapEntryIteratorObjectIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@(0)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapEntryIteratorObjectIterator(
			$this->getMockForAbstractClass(LdapObjectFactoryInterface::class),
			[],
			[],
			0,
		);
	}
	
}
