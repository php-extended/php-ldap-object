<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapRecord;
use PHPUnit\Framework\TestCase;

/**
 * LdapRecordTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapRecord
 *
 * @internal
 *
 * @small
 */
class LdapRecordTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapRecord
	 */
	protected LdapRecord $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{'.\get_class($this->_object).'@c=com,uid=dn}', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(LdapRecord::class, [
			new LdapDistinguishedName(['uid' => 'dn', 'c' => 'com']), 
			['objectClass'],
		]);
	}
	
}
