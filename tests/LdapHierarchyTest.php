<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapHierarchy;
use PhpExtended\Ldap\LdapRecordInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapHierarchyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapHierarchy
 *
 * @internal
 *
 * @small
 */
class LdapHierarchyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapHierarchy
	 */
	protected LdapHierarchy $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapHierarchy(
			$this->getMockForAbstractClass(LdapRecordInterface::class),
			new LdapDistinguishedName(),
		);
	}
	
}
