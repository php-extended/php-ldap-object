<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapEntry;
use PHPUnit\Framework\TestCase;

/**
 * LdapEntryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapDistinguishedName
 *
 * @internal
 *
 * @small
 */
class LdapEntryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapEntry
	 */
	protected LdapEntry $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('uid=myself,ou=sector,org=company,c=com [top, person]', $this->_object->__toString());
	}
	
	public function testGetDn() : void
	{
		$this->assertEquals(new LdapDistinguishedName([
			'c' => 'com', 'org' => 'company', 'ou' => 'sector', 'uid' => 'myself',
		]), $this->_object->getDn());
	}
	
	public function testHasObjectClass() : void
	{
		$this->assertTrue($this->_object->hasObjectClass('top'));
	}
	
	public function testGetObjectClasses() : void
	{
		$this->assertEquals(['top', 'person'], $this->_object->getObjectClasses());
	}
	
	public function testHasAttribute() : void
	{
		$this->assertTrue($this->_object->hasAttribute('cn'));
	}
	
	public function testGetAttribute() : void
	{
		$this->assertEquals(['Simple Name'], $this->_object->getAttribute('sn'));
	}
	
	public function testGetAttributeArray() : void
	{
		$this->assertEquals(['test@example.com', 'foobar@quux.com'], $this->_object->getAttribute('emails'));
	}
	
	public function testGetAllAttributes() : void
	{
		$this->assertEquals([
			'sn' => ['Simple Name'],
			'cn' => ['Common Name'],
			'emails' => ['test@example.com', 'foobar@quux.com'],
		], $this->_object->getAttributes());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapEntry(new LdapDistinguishedName([
			'c' => 'com', 'org' => 'company', 'ou' => 'sector', 'uid' => 'myself',
		]), [
			'sn' => [
				'count' => 1,
				0 => 'Simple Name',
			],
			0 => 'sn',
			'cn' => [
				'count' => 1,
				0 => 'Common Name',
			],
			1 => 'cn',
			'objectClass' => [
				'count' => 2,
				0 => 'top',
				1 => 'person',
			],
			2 => 'objectClass',
			'emails' => [
				'count' => 2,
				0 => 'test@example.com',
				1 => 'foobar@quux.com',
			],
			3 => 'emails',
			'count' => 4,
		]);
	}
	
}
