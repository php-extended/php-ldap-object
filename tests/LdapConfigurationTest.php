<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapConfiguration;
use PhpExtended\Ldap\LdapDistinguishedName;
use PHPUnit\Framework\TestCase;

/**
 * LdapConfigurationTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapConfiguration
 *
 * @internal
 *
 * @small
 */
class LdapConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapConfiguration
	 */
	protected LdapConfiguration $_object;
	
	public function testDefaultToString() : void
	{
		$this->assertEquals('ldaps://(anonymous)@host.name.test:389', $this->_object->__toString());
	}
	
	public function testDefaultHostname() : void
	{
		$this->assertEquals('host.name.test', $this->_object->getHostname());
	}
	
	public function testDefaultPort() : void
	{
		$this->assertEquals(389, $this->_object->getPort());
	}
	
	public function testDefaultIsSecure() : void
	{
		$this->assertTrue($this->_object->isSecure());
	}
	
	public function testDefaultConnectionTimeout() : void
	{
		$this->assertEquals(10, $this->_object->getConnectionTimeout());
	}
	
	public function testDefaultSuffixDn() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_object->getSuffixDn());
	}
	
	public function testDefaultLogin() : void
	{
		$this->assertNull($this->_object->getLogin());
	}
	
	public function testDefaultPassword() : void
	{
		$this->assertNull($this->_object->getPassword());
	}
	
	public function testDefaultVersion() : void
	{
		$this->assertEquals(3, $this->_object->getVersion());
	}
	
	public function testDefaultFollowReferral() : void
	{
		$this->assertFalse($this->_object->followsReferral());
	}
	
	public function testDefaultDereferencePolicy() : void
	{
		$this->assertEquals(0, $this->_object->getDereferencePolicy());
	}
	
	public function testDefaultSizeLimit() : void
	{
		$this->assertEquals(0, $this->_object->getSizeLimit());
	}
	
	public function testDefaultTimeLimit() : void
	{
		$this->assertEquals(0, $this->_object->getTimeLimit());
	}
	
	public function testDefaultServicepingEnabled() : void
	{
		$this->assertTrue($this->_object->isServicePingEnabled());
	}
	
	public function testToString() : void
	{
		$this->_object->setLogin('myusername');
		$this->assertEquals('ldaps://myusername@host.name.test:389', $this->_object->__toString());
	}
	
	public function testHostname() : void
	{
		$this->_object->setHostname('new.host.name');
		$this->assertEquals('new.host.name', $this->_object->getHostname());
	}
	
	public function testPort() : void
	{
		$this->_object->setPort(448);
		$this->assertEquals(448, $this->_object->getPort());
	}
	
	public function testIsSecure() : void
	{
		$this->_object->setSecure(false);
		$this->assertFalse($this->_object->isSecure());
	}
	
	public function testConnectionTimeout() : void
	{
		$this->_object->setConnectionTimeout(60);
		$this->assertEquals(60, $this->_object->getConnectionTimeout());
	}
	
	public function testSuffixDn() : void
	{
		$this->_object->setSuffixDn(new LdapDistinguishedName(['uid' => 'toto']));
		$this->assertEquals(new LdapDistinguishedName(['uid' => 'toto']), $this->_object->getSuffixDn());
	}
	
	public function testLogin() : void
	{
		$this->_object->setLogin('foobar');
		$this->assertEquals('foobar', $this->_object->getLogin());
	}
	
	public function testPassword() : void
	{
		$this->_object->setPassword('foobar');
		$this->assertEquals('foobar', $this->_object->getPassword());
	}
	
	public function testVersion() : void
	{
		$this->_object->setVersion(2);
		$this->assertEquals(2, $this->_object->getVersion());
	}
	
	public function testVersionFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->setVersion(14);
	}
	
	public function testFollowReferral() : void
	{
		$this->_object->setFollowsReferral(true);
		$this->assertTrue($this->_object->followsReferral());
	}
	
	public function testDereferencePolicy() : void
	{
		$this->_object->setDereferencePolicy(3);
		$this->assertEquals(3, $this->_object->getDereferencePolicy());
	}
	
	public function testDereferencePolicyFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->setDereferencePolicy(14);
	}
	
	public function testSizeLimit() : void
	{
		$this->_object->setSizeLimit(100);
		$this->assertEquals(100, $this->_object->getSizeLimit());
	}
	
	public function testSizeLimitFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->setSizeLimit(-100);
	}
	
	public function testTimeLimit() : void
	{
		$this->_object->setTimeLimit(60);
		$this->assertEquals(60, $this->_object->getTimeLimit());
	}
	
	public function testTimeLimitFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->setTimeLimit(-100);
	}
	
	public function testServicepingEnabled() : void
	{
		$this->_object->setServicePingEnabled(false);
		$this->assertFalse($this->_object->isServicePingEnabled());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapConfiguration('host.name.test', new LdapDistinguishedName());
	}
	
}
