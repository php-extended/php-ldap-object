<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * LdapObjectFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapObjectFactory
 *
 * @internal
 *
 * @small
 */
class LdapObjectFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapObjectFactory
	 */
	protected LdapObjectFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(LdapObjectFactory::class);
	}
	
}
