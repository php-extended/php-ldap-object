<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapMemoryUserSession;
use PHPUnit\Framework\TestCase;

/**
 * LdapMemoryUserSessionTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapMemoryUserSession
 *
 * @internal
 *
 * @small
 */
class LdapMemoryUserSessionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapMemoryUserSession
	 */
	protected LdapMemoryUserSession $_object;
	
	public function testCreate() : void
	{
		$this->assertEquals($this->_object, LdapMemoryUserSession::create('username', 'p@ssword'));
	}
	
	public function testLoad() : void
	{
		$this->assertEquals(new LdapMemoryUserSession(), LdapMemoryUserSession::load());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('username', $this->_object->__toString());
	}
	
	public function testIsAnonymous() : void
	{
		$this->assertFalse($this->_object->isAnonymous());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('username', $this->_object->getIdentifier());
	}
	
	public function testGetPassword() : void
	{
		$this->assertEquals('p@ssword', $this->_object->getPassword());
	}
	
	public function testSave() : void
	{
		$this->assertTrue($this->_object->save());
	}
	
	public function testLogout() : void
	{
		$this->assertTrue($this->_object->logout());
		$this->assertNull($this->_object->getIdentifier());
		$this->assertNull($this->_object->getPassword());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapMemoryUserSession('username', 'p@ssword');
	}
	
}
