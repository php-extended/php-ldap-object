<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapCriteria;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapRecursiveCachedEntryIterator;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * LdapRecursiveCachedEntryIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapRecursiveCachedEntryIterator
 *
 * @internal
 *
 * @small
 */
class LdapRecursiveCachedEntryIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapRecursiveCachedEntryIterator
	 */
	protected LdapRecursiveCachedEntryIterator $_object;
	
// 	public function testToString() : void
// 	{
// 		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
// 	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
// 		$this->_object = new LdapRecursiveCachedEntryIterator(
// 			new LdapDistinguishedNameParser(),
// 			\ldap_connect('ldap://hostname.example:389'),
// 			new LdapDistinguishedName(),
// 			new LdapCriteria(),
// 			new NullLogger()
// 		);
	}
	
}
