<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapConfiguration;
use PhpExtended\Ldap\LdapConnection;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapObjectFactoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapConnectionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapConnection
 *
 * @internal
 *
 * @small
 */
class LdapConnectionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapConnection
	 */
	protected LdapConnection $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[]::ldaps://(anonymous)@host.name:389', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapConnection(
			new LdapConfiguration('host.name', new LdapDistinguishedName()),
			new LdapDistinguishedNameParser(),
			$this->getMockForAbstractClass(LdapObjectFactoryInterface::class),
		);
	}
	
}
