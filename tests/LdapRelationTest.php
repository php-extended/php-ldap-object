<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapArrayObjectIterator;
use PhpExtended\Ldap\LdapConnection;
use PhpExtended\Ldap\LdapCriteria;
use PhpExtended\Ldap\LdapCriteriaInterface;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapObjectIteratorInterface;
use PhpExtended\Ldap\LdapRecord;
use PhpExtended\Ldap\LdapRelation;
use PhpExtended\Ldap\LdapRelationInterface;
use PHPUnit\Framework\TestCase;

class LdapRelationLdapRecordSource extends LdapRecord
{
	// nothing to add
}

class LdapRelationLdapRecordTarget extends LdapRecord
{
	// nothing to add
}

class LdapRelationLdapConnection extends LdapConnection
{
	public function __construct() {}
}

class LdapRelationLdapRecord extends LdapRecord
{
	
	public function __get(string $fieldname)
	{ 
		return ('empty' === $fieldname) ? '' : 'foobar : '.$fieldname;
	}
	
}

/**
 * LdapRelationTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapRelation
 *
 * @internal
 *
 * @small
 */
class LdapRelationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapRelation
	 */
	protected LdapRelation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Ldap\\LdapRelation(n-n)[LdapRelationLdapRecordSource|sourceField => LdapRelationLdapRecordTarget|targetField]', $this->_object->__toString());
	}
	
	public function testTypeOfRelation() : void
	{
		$this->assertEquals(LdapRelationInterface::MANY_TO_MANY, $this->_object->getTypeOfRelation());
	}
	
	public function testOtherTypeOfRelation() : void
	{
		$this->_object = new LdapRelation(LdapRelationInterface::ONE_TO_MANY, '', '', '', '');
		
		$this->assertEquals(LdapRelationInterface::ONE_TO_MANY, $this->_object->getTypeOfRelation());
	}
	
	public function testSourceClass() : void
	{
		$this->assertEquals(LdapRelationLdapRecordSource::class, $this->_object->getSourceClass());
	}
	
	public function testSourceField() : void
	{
		$this->assertEquals('sourceField', $this->_object->getSourceField());
	}
	
	public function testTargetClass() : void
	{
		$this->assertEquals(LdapRelationLdapRecordTarget::class, $this->_object->getTargetClass());
	}
	
	public function testTargetField() : void
	{
		$this->assertEquals('targetField', $this->_object->getTargetField());
	}
	
	public function testRelatedObjects() : void
	{
		$record = new LdapRelationLdapRecord(new LdapDistinguishedName(['uuid' => '0123']), ['objectClass']);
		
		$expected = new LdapArrayObjectIterator([
			new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
		]);
		
		$this->assertEquals($expected, $this->_object->getRelatedObjects($record));
	}
	
	public function testRelatedObjectsByDn() : void
	{
		$expected = new LdapArrayObjectIterator([
			new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
			new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
		]);
		
		$dns = [new LdapDistinguishedName(), new LdapDistinguishedName()];
		
		$this->assertEquals($expected, $this->_object->getRelatedObjectsByDn($dns, new LdapCriteria()));
	}
	
	public function testRelatedObjectsWithDn() : void
	{
		$this->_object = new LdapRelation(LdapRelationInterface::MANY_TO_MANY, LdapRelationLdapRecordSource::class, 'dn', LdapRelationLdapRecordTarget::class, 'target');
		
		$expected = new LdapArrayObjectIterator([
			new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
		]);
		
		$record = new LdapRelationLdapRecordSource(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']);
		
		$this->assertEquals($expected, $this->_object->getRelatedObjects($record, new LdapCriteria()));
	}
	
	public function testRelatedObjectsWithEmpty() : void
	{
		LdapRecord::$ldap = new class() extends LdapHierarchyTestLdapConnection
		{
			
			public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
			{
				return new LdapArrayObjectIterator();
			}
			
		};
		
		$this->_object = new LdapRelation(LdapRelationInterface::MANY_TO_MANY, LdapRelationLdapRecordSource::class, 'empty', LdapRelationLdapRecordTarget::class, 'targetField');
		
		$record = new LdapRelationLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']);
		
		$this->assertEquals(new LdapArrayObjectIterator(), $this->_object->getRelatedObjects($record, new LdapCriteria()));
	}
	
	public function testRelatedObjectsTargetDn() : void
	{
		$this->_object = new LdapRelation(LdapRelationInterface::MANY_TO_MANY, LdapRelationLdapRecordSource::class, 'sourceField', LdapRelationLdapRecordTarget::class, 'dn');
		
		$record = new LdapRelationLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']);
		
		$expected = new LdapArrayObjectIterator([
			new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
		]);
		
		$this->assertEquals($expected, $this->_object->getRelatedObjects($record));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		LdapRecord::$ldap = new class() extends LdapHierarchyTestLdapConnection
		{
			
			public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface
			{
				return new LdapArrayObjectIterator([
					new LdapHierarchyTestLdapRecord(new LdapDistinguishedName(['cn' => 'barfoo']), ['objectClass']),
				]);
			}
			
		};
		
		$this->_object = new LdapRelation(LdapRelationInterface::MANY_TO_MANY, LdapRelationLdapRecordSource::class, 'sourceField', LdapRelationLdapRecordTarget::class, 'targetField');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		LdapRecord::$ldap = null;
	}
	
}
