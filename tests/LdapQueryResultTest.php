<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapQueryResult;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * LdapQueryResultTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapQueryResult
 *
 * @internal
 *
 * @small
 */
class LdapQueryResultTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapQueryResult
	 */
	protected LdapQueryResult $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('RESULT : 0 (OFFSET 0 LIMIT 1000)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapQueryResult(
			\ldap_connect('ldap://hostname.example:389'),
			null,
			new LdapDistinguishedNameParser(),
			new NullLogger(),
		);
	}
	
}
