<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapCriteria;
use PhpExtended\Ldap\LdapCriteriaInterface;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeMultiInterface;
use PhpExtended\Ldap\LdapFilterNodeNot;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PhpExtended\Ldap\LdapRecord;
use PHPUnit\Framework\TestCase;

class test_get_all_filters extends LdapRecord
{
	
	public static function getLdapObjectClass() : string
	{
		return 'test_get_all';
	}
	
	public static function getLdapClassCriteriaFilters() : array
	{
		return [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'quux', 'foobar')];
	}
}

/**
 * LdapCriteriaTest calass file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapCriteria
 *
 * @internal
 *
 * @small
 */
class LdapCriteriaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapCriteria
	 */
	protected LdapCriteria $_object;
	
	public function testToString() : void
	{
		$this->_object->setBaseDn(new LdapDistinguishedName(['cn' => 'foobar']));
		$expected = <<<EOF
PhpExtended\\Ldap\\LdapCriteria
BASES [
	cn=foobar
]
RECORD NULL
OFFSET 0
LIMIT 1000
SCOPE 0 (BASE)
FILTER (empty)
EOF;
		$this->assertEquals($expected, $this->_object->__toString());
	}
	
	public function testDefaultIsEmpty() : void
	{
		$this->assertTrue($this->_object->isEmpty());
	}
	
	public function testDefaultOffset() : void
	{
		$this->assertEquals(0, $this->_object->getOffset());
	}
	
	public function testSetOffset() : void
	{
		$this->_object->setOffset(-1);
		
		$this->assertEquals(0, $this->_object->getOffset());
	}
	
	public function testDefaultLimit() : void
	{
		$this->assertEquals(1000, $this->_object->getLimit());
	}
	
	public function testSetLimit() : void
	{
		$this->_object->setLimit(-1);
		
		$this->assertEquals(1000, $this->_object->getLimit());
	}
	
	public function testDefaultScope() : void
	{
		$this->assertEquals(LdapCriteriaInterface::LDAP_SCOPE_BASE, $this->_object->getScope());
	}
	
	public function testSetScope() : void
	{
		$this->_object->setScope(-1);
		
		$this->assertEquals(LdapCriteriaInterface::LDAP_SCOPE_BASE, $this->_object->getScope());
	}
	
	public function testSetScope2() : void
	{
		$this->_object->setScope(4);
		
		$this->assertEquals(LdapCriteriaInterface::LDAP_SCOPE_SUBTREE, $this->_object->getScope());
	}
	
	public function testDefaultRecordClass() : void
	{
		$this->assertNull($this->_object->getRecordClass());
	}
	
	public function testDefaultBaseDns() : void
	{
		$this->assertEquals([], $this->_object->getBaseDns());
	}
	
	public function testDefaultFilter() : void
	{
		$this->assertEquals('', $this->_object->getFilters()->__toString());
	}
	
	public function testIsEmptyBaseDn() : void
	{
		$this->_object->setBaseDn(new LdapDistinguishedName(['uid' => 'toto']));
		
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testIsEmptyRecordClass() : void
	{
		$this->_object->setRecordClass('recordClass');
		
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testIsNotEmpty() : void
	{
		$this->_object->addValue('column', 'value');
		
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testAddMultipleDn() : void
	{
		$this->_object->addAllBaseDns([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo']]),
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'toto']]),
		]);
		
		$this->assertEquals([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'toto']]),
		], $this->_object->getBaseDns());
	}
	
	public function testAddMultipleDn2() : void
	{
		$this->_object->addAllBaseDns([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'toto']]),
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo']]),
		]);
		
		$this->assertEquals([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'toto']]),
		], $this->_object->getBaseDns());
	}
	
	public function testAddBaseDn() : void
	{
		$this->_object->addBaseDn(new LdapDistinguishedName());
		
		$this->assertEquals([], $this->_object->getBaseDns());
	}
	
	public function testSetBaseDn() : void
	{
		$this->_object->addAllBaseDns([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'toto']]),
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo'], ['uid', 'foobar']]),
		]);
		$this->_object->setBaseDn(new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo']]));
		
		$this->assertEquals([
			new LdapDistinguishedName([['ou', 'foo'], ['bar', 'baz'], ['qux', 'boo']]),
		], $this->_object->getBaseDns());
	}
	
	public function testSetFilters() : void
	{
		$this->_object->setFilters($expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'baz', 'qux'),
		]));
		
		$this->assertEquals($expected, $this->_object->getFilters());
	}
	
	public function testAddFilterNode() : void
	{
		$this->_object->addFilterNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
		]), $this->_object->getFilters());
	}
	
	public function testAddFilterNodes() : void
	{
		$this->_object->addFilterNodes([new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar')]);
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
		]), $this->_object->getFilters());
	}
	
	public function testAddValue() : void
	{
		$this->_object->addValue('foo', 'bar');
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
		]), $this->_object->getFilters());
	}
	
	public function testAddValues() : void
	{
		$this->_object->addValues('foo', ['bar']);
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_OR, [
				new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
			]),
		]), $this->_object->getFilters());
	}
	
	public function testAddNotValue() : void
	{
		$this->_object->addNotValue('foo', 'bar');
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeNot(
				new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
			),
		]), $this->_object->getFilters());
	}
	
	public function testAddNotValues() : void
	{
		$this->_object->addNotValues('foo', ['bar']);
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeNot(
				new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_OR, [
					new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
				]),
			),
		]), $this->_object->getFilters());
	}
	
	public function testRemoveFilters() : void
	{
		$this->_object->addValue('foo', 'bar');
		
		$this->_object->removeFiltersFor('foo');
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, []), $this->_object->getFilters());
	}
	
	public function testGetAllFilters() : void
	{
		$this->_object->addValue('foo', 'bar');
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'),
		]), $this->_object->getAllFilters());
	}
	
	public function testGetAllFiltersWithClass() : void
	{
		$this->_object->setRecordClass(test_get_all_filters::class);
		
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'objectClass', 'test_get_all'),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'quux', 'foobar'),
		]), $this->_object->getAllFilters());
	}
	
	public function testMergeWith() : void
	{
		$this->_object->setOffset(10);
		$this->_object->setLimit(10);
		$this->_object->setRecordClass('recordClass');
		$this->_object->setScope(LdapCriteriaInterface::LDAP_SCOPE_BASE);
		$this->_object->addBaseDn(new LdapDistinguishedName(['cn' => 'foo', 'ou' => 'bar', 'c' => 'baz']));
		$this->_object->addValue('foobar', 'quuxfoo');
		
		$newCriteria = new LdapCriteria();
		$newCriteria->setOffset(15);
		$newCriteria->setLimit(15);
		$newCriteria->setRecordClass('newClass');
		$newCriteria->setScope(LdapCriteria::LDAP_SCOPE_ONE);
		$newCriteria->addBaseDn(new LdapDistinguishedName(['cn' => 'baz', 'ou' => 'foo', 'c' => 'bar']));
		$newCriteria->addValue('quux', 'foobar');
		
		$expected = new LdapCriteria();
		$expected->setOffset(15);
		$expected->setLimit(15);
		$expected->setRecordClass('newClass');
		$expected->setScope(LdapCriteria::LDAP_SCOPE_ONE);
		$expected->addAllBaseDns([
			new LdapDistinguishedName(['cn' => 'foo', 'ou' => 'bar', 'c' => 'baz']),
			new LdapDistinguishedName(['cn' => 'baz', 'ou' => 'foo', 'c' => 'bar']),
		]);
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addValue('foobar', 'quuxfoo');
		$expected->getFilters()->addNode($node);
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addValue('quux', 'foobar');
		$expected->getFilters()->addNode($node);
		
		$this->assertEquals($expected, $this->_object->mergeWith($newCriteria));
		
		$expected->setRecordClass('recordClass');
		$expected->setAllBaseDns([
			new LdapDistinguishedName(['cn' => 'baz', 'ou' => 'foo', 'c' => 'bar']),
			new LdapDistinguishedName(['cn' => 'foo', 'ou' => 'bar', 'c' => 'baz']),
		]);
		$expected->setFilters(new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND));
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addValue('quux', 'foobar');
		$expected->getFilters()->addNode($node);
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addValue('foobar', 'quuxfoo');
		$expected->getFilters()->addNode($node);
		
		$this->assertEquals($expected, $newCriteria->mergeWith($this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapCriteria();
	}
	
}
